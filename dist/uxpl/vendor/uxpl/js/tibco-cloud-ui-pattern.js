// $(document).ready(function () {
//     console.log("Inside dropdown");
//     if($(".tc-dropdown > .dropdown-menu").length){
//         $(".tc-dropdown > .dropdown-menu").mCustomScrollbar({
//             scrollbarPosition:"inside"
//         });
//     }
// });
/**
 * Created by hede on 10/13/2017.
 */
// $(document).ready(function () {
//     console.log("Inside dropdown new");
//     /* if($(".tc-dropdown > .dropdown-menu").length){
//      $(".tc-dropdown > .dropdown-menu").mCustomScrollbar({
//      scrollbarPosition:"inside"
//      });
//      }
//      */
//     $("body").click(function(ev){
//         console.log("clicked out");
//         if ($(".dropdown-selector").length) {
//             console.log("inside");
//             if (!ev.target.parentNode.matches('.dropdown-selector')) {
//                 /*  if (!($(ev).hasClass('.dropdown-selector'))) {*/
//                 console.log("target not match");
//                 var dropdowns = document.getElementsByClassName("dropdown-menu");
//                 var i;
//                 for (i = 0; i < dropdowns.length; i++) {
//                     var openDropdown = dropdowns[i];
//                     if (openDropdown.classList.contains('tc-dd-show')) {
//                         console.log("openDropdown if");
//                         openDropdown.classList.remove('tc-dd-show');
//                     }
//                 }
//             }
//         }
//     });
//
// });

// var addClickEventListener = function (classname, callBackFunction) {
//     for (var i = 0; i < classname.length; i++) {
//         classname[i].addEventListener('click', callBackFunction, false);
//     }
// }
// var bodyCBFunction = function (event) {



// function openDropDownIconMenu() {
//     console.log("Inside openDropDown" + document.querySelector(".dropdown-icon-list"));
//     document.querySelector(".dropdown-icon-list").style.display = "block";
// }
// $(".tc-global-header-nav").click(function (event) {
//     event.stopPropagation();
//     $('.tc-container-background,.tc-panel-background').show();
//     $('.tc-icon-tray').addClass('active');
//     $('.tc-panel-container').addClass('slide-in');
// });
// if ($(".tc-global-header-container").length) {
//     $('body').click(function () {
//         $('.tc-container-background,.tc-panel-background').hide();
//         $('.tc-icon-tray').removeClass('active');
//         $('.tc-panel-container').removeClass('slide-in');
//         $(".tc-header-nav-item").removeClass('active');
//     });
// }
var getClassName = function (className) {
    return document.getElementsByClassName(className);
}
var tcHeaderNavItem = getClassName("tc-header-nav-item");
var tcGlobalHeaderNav = getClassName("tc-global-header-nav");

var navItemCbFn = function () {
    removeClass(tcHeaderNavItem, "active");
    var li = event.target.tagName.toLowerCase() === 'li' ? event.target : event.target.closest('li');
    li.classList.add("active");
    console.log("li", li);
    var item = li.classList[1];
    console.log("item", item);
    // document.getElementsByClassName("tc-panel-content " + item)[0].classList.add("active");
}
var addClass = function (element, className) {
    for (var i = 0; i < element.length; i++) {
        element[i].classList.add(className);
    }
}
var removeClass = function (element, className) {
    for (var i = 0; i < element.length; i++) {
        element[i].classList.remove(className);
    }
}


var navCBFunction = function (event) {
    event.stopPropagation();
    var showElement = document.getElementsByClassName('tc-container-background');
    var showElem_bg = document.getElementsByClassName('tc-panel-background');

    // for (var i = 0; i < showElement.length; i++) {
    showElement[0].style.display = 'block';
    // }
    // for (var j = 0; j < showElem_bg.length; i++) {
    showElem_bg[0].style.display = 'block';
    // }
    addClass(document.getElementsByClassName('tc-icon-tray'), "active")
    addClass(document.getElementsByClassName('tc-panel-container'), "slide-in")

}

var addClickEventListener = function (classname, callBackFunction) {
    for (var i = 0; i < classname.length; i++) {
        classname[i].addEventListener('click', callBackFunction, false);
    }
}
addClickEventListener(tcHeaderNavItem, navItemCbFn);
addClickEventListener(tcGlobalHeaderNav, navCBFunction);

/**
 * @license
 * Copyright 2015-2016 TIBCO Inc. All Rights Reserved.
 */
$(function(){
	var infoQty = '';
	var infoLbl = '';
	var infoLblRunning = '';

	// Status Stopped
	/*$(".tc-indicators-circle-status-stopped").hover(function () {
		setStartStyleHover(this);
	}, function () {
		setStartStyleUnhover(this);
	});*/

	// Status Stopped - on click
	$(".tc-indicators-circle-status-stopped").on('click', function () {
		$(".tc-indicators-circle-status-stopped").removeClass("status-stopped-start");
		$(this).removeClass('status-stopped').addClass('status-stopped-scale');
		setStartInstancesStyle();
	});

	// tc-indicator-caret-up
	$(".tc-indicators-caret-up-status-stopped").on('click', function () {
		scaleUp(".tc-indicators-circle-status-stopped.status-stopped-scale", "tc-indicators-caret-down-status-stopped", "tc-indicators-caret-up-status-stopped");
	});

	// tc-indicator-caret-up
	$(".tc-indicators-caret-up-status-running").on('click', function () {
		scaleUp(".tc-indicators-circle-status-running.status-to-running-scale", "tc-indicators-caret-down-status-running", "tc-indicators-caret-up-status-running");
	});

	//.tc-indicators-error-caret-up
	$(".tc-indicators-caret-up-status-error").on('click', function () {
		scaleUp(".tc-indicators-circle-status-error.status-to-error-scale", "tc-indicators-caret-down-status-error", "tc-indicators-caret-up-status-error");
	});

	// tc-indicator-caret-down
	$(".tc-indicators-caret-down-status-stopped").on('click', function () {
		scaleDown(".tc-indicators-circle-status-stopped.status-stopped-scale", "tc-indicators-caret-down-status-stopped", "tc-indicators-caret-up-status-stopped");
	});

	// tc-indicator-caret-down
	$(".tc-indicators-caret-down-status-running").on('click', function () {
		scaleDown(".tc-indicators-circle-status-running.status-to-running-scale", "tc-indicators-caret-down-status-running", "tc-indicators-caret-up-status-running");
	});

	// tc-indicator-caret-down
	$(".tc-indicators-caret-down-status-error").on('click', function () {
		scaleDown(".tc-indicators-circle-status-error.status-to-error-scale", "tc-indicators-caret-down-status-error", "tc-indicators-caret-up-status-error");
	});

	// Status Running
	/*$(".tc-indicators-circle-status-running").hover(function () {
		if(!$(this).hasClass("status-to-running-scale")) {
			infoLblRunning = 
				infoLblRunning == '' 
					? $(".tc-indicators-circle-status-running .info-lbl-instances-status-running>span.lbl-status-running").text() : infoLblRunning;

			$(".tc-indicators-circle-status-running").addClass('status-running-scale');
			$(".tc-indicators-circle-status-running .info-lbl-instances-status-running>span.lbl-status-running").text('Scale instances');
			$(".tc-indicators-circle-status-running.status-running-scale .info-lbl-instances-status-running").addClass('info-lbl-instances-ext');
		}
	}, function () {

		if(!$(this).hasClass("status-to-running-scale")) {
			
			$(".tc-indicators-circle-status-running.status-running-scale").removeClass('status-running-scale');
			$(".info-lbl-instances-status-running").addClass('status-running');
			$(".tc-indicators-circle-status-running .info-lbl-instances-status-running").removeClass('info-lbl-instances-ext');
			$(".tc-indicators-circle-status-running .info-lbl-instances-status-running>span.lbl-status-running").text(infoLblRunning);
			
			infoLblRunning = '';
		}

	});*/

	// Status Error
	/*$(".tc-indicators-circle-status-error").hover(function () {
		if(!$(this).hasClass("status-to-error-scale")) {
			infoLblRunning = 
				infoLblRunning == '' 
					? $(".tc-indicators-circle-status-error .info-lbl-instances-status-error>span.lbl-status-error").text() : infoLblRunning;

			$(".tc-indicators-circle-status-error").addClass('status-error-scale');
			$(".tc-indicators-circle-status-error .info-lbl-instances-status-error>span.lbl-status-error").text('Scale instances');
			$(".tc-indicators-circle-status-error.status-to-error-scale .info-lbl-instances-status-error").addClass('info-lbl-instances-ext');
		}
	}, function () {

		if(!$(this).hasClass("status-to-error-scale")) {
			
			$(".tc-indicators-circle-status-error.status-error-scale").removeClass('status-error-scale');
			$(".info-lbl-instances-status-error").addClass('status-error');
			$(".tc-indicators-circle-status-error .info-lbl-instances-status-error").removeClass('info-lbl-instances-ext');
			$(".tc-indicators-circle-status-error .info-lbl-instances-status-error>span.lbl-status-error").text(infoLblRunning);
			
			infoLblRunning = '';
		}

	});*/

	// Status Running - on click
	$(".tc-indicators-circle-status-running").on('click', function () {
		$(this).removeClass('status-running');
		$(this).addClass("status-to-running-scale");
		
		$(".tc-indicators-circle-status-running.status-to-running-scale > .info-lbl-instances-status-running")
			.addClass('info-lbl-instances-ext');
		
		$(".tc-indicators-circle-status-running.status-to-running-scale > div.tc-indicators-caret-up-status-running")
			.addClass("tc-indicator-running-caret-up");
		
		$(".tc-indicators-circle-status-running.status-to-running-scale > div.tc-indicators-caret-down-status-running")
			.addClass("tc-indicator-running-caret-down");

		$(".tc-indicators-card > .tc-card-border-status-running").addClass("selected-running");

		$(".tc-indicators-card .tc-indicators-circle-status-running").addClass("tc-circle-no-border");

		var baseElem = ".tc-indicators-circle-status-running.status-to-running-scale";
		var instances = getQtyInstances(baseElem);

		if (instances > 0 && instances <= 3) {
			addScaleClass(".tc-indicators-circle-status-running.status-to-running-scale");
		} else {
			removeScaleClass(".tc-indicators-circle-status-running.status-to-running-scale");
		}
	});

	// Status Error - on click
	$(".tc-indicators-circle-status-error").on('click', function () {
		$(this).removeClass('status-error');
		$(this).addClass("status-to-error-scale");
		
		$(".tc-indicators-circle-status-error.status-to-error-scale > .info-lbl-instances-status-error")
			.addClass('info-lbl-instances-ext');
		
		$(".tc-indicators-circle-status-error.status-to-error-scale > div.tc-indicators-caret-up-status-error")
			.addClass("tc-indicator-error-caret-up");
		
		$(".tc-indicators-circle-status-error.status-to-error-scale > div.tc-indicators-caret-down-status-error")
			.addClass("tc-indicator-error-caret-down");

		$(".tc-indicators-card > .tc-card-border-status-error").addClass("selected-error");

		$(".tc-indicators-card .tc-indicators-circle-status-error").addClass("tc-circle-no-border");

		var baseElem = ".tc-indicators-circle-status-error.status-to-error-scale";
		var instances = getQtyInstances(baseElem);

		if (instances > 0 && instances <= 3) {
			addScaleFromErrorClass(".tc-indicators-circle-status-error.status-to-error-scale");
		} else {
			removeScaleFromErrorClass(".tc-indicators-circle-status-error.status-to-error-scale");
		}
	});

	$(".info-lbl-instances-status-running").on('click', function () {
		if ($(".tc-indicators-circle-status-running").hasClass("status-to-running-scale") && $(".info-lbl-instances-status-running").hasClass("to-running-scale")) {
			$(this).addClass("scaling");
			$(".lbl-status-running").addClass("hidden");
			$(".lbl-acting-status-running").removeAttr("hidden");
			$(".tc-indicators-caret-up-status-running").addClass("hidden");
			$(".tc-indicators-caret-down-status-running").addClass("hidden");
			$(".tc-indicators-circle-status-running.status-running-scale.status-to-running-scale").addClass("tc-running-spinner");
		}
	});

	$(".info-lbl-instances-status-error").on('click', function () {
		if ($(".tc-indicators-circle-status-error").hasClass("status-to-error-scale") && $(".info-lbl-instances-status-error").hasClass("to-error-scale")) {
			$(this).addClass("scaling");
			$(".lbl-status-error").addClass("hidden");
			$(".lbl-acting-status-error").removeAttr("hidden");
			$(".tc-indicators-caret-up-status-error").addClass("hidden");
			$(".tc-indicators-caret-down-status-error").addClass("hidden");
			$(".tc-indicators-circle-status-error.status-error-scale.status-to-error-scale").addClass("tc-error-spinner");
		}
	});

	$(".info-lbl-instances-status-stopped").on('click', function () {
		if ($(".tc-indicators-circle-status-stopped").hasClass("status-stopped-scale") && $(".info-lbl-instances-status-stopped").hasClass("to-stopped-scale")) {
			$(this).addClass("scaling");
			$(".lbl-status-stopped").addClass("hidden");
			$(".lbl-acting-status-stopped").removeAttr("hidden");
			$(".tc-indicators-caret-up-status-stopped").addClass("hidden");
			$(".tc-indicators-caret-down-status-stopped").addClass("hidden");
			$(".tc-indicators-circle-status-stopped.status-stopped-scale").addClass("tc-stopped-spinner");
		} 
	});

	function setStartInstancesStyle () {
		if (!$(".tc-indicators-caret-down-status-stopped").hasClass('tc-indicator-stopped-caret-down')) {
			$(".status-stopped-scale .info-quantity-instances>span").text('1');
		}

		$(".status-stopped-scale .info-lbl-instances-status-stopped>span.lbl-status-stopped").text('Start instances');

		$(".status-stopped-scale .info-lbl-instances-status-stopped").addClass('info-lbl-instances-ext');

		$(".status-stopped-scale .tc-indicators-caret-up-status-stopped").addClass("tc-indicator-stopped-caret-up");
		$(".status-stopped-scale .tc-indicators-caret-down-status-stopped").addClass("tc-indicator-stopped-caret-down");

		$(".tc-indicators-card > .tc-card-border-status-stopped").addClass("selected-stopped");

		$(".tc-indicators-card .tc-indicators-circle-status-stopped").addClass("tc-circle-no-border");

		var baseElem = ".tc-indicators-circle-status-stopped.status-stopped-scale";
		var instances = getQtyInstances(baseElem);

		if (instances > 0 && instances <= 3) {
			addStartClass(".tc-indicators-circle-status-stopped.status-stopped-scale");
		} else {
			removeStartClass(".tc-indicators-circle-status-stopped.status-stopped-scale");
		}
	}

	function setStartStyleHover(elem) {
		if (!$(elem).hasClass('status-stopped-scale')) {
			infoQty = infoQty == '' 
				? $(".tc-indicators-circle-status-stopped .info-quantity-instances>span").text() : infoQty;
			infoLbl = infoLbl == '' 
				? $(".tc-indicators-circle-status-stopped .info-lbl-instances-status-stopped>span.lbl-status-stopped").text() : infoLbl;
			$(elem).removeClass('status-stopped').addClass('status-stopped-start');
			$(".status-stopped-start .info-quantity-instances>span").text('Start');
			$(".status-stopped-start .info-lbl-instances-status-stopped>span.lbl-status-stopped").text('Start instances');
			$(".status-stopped-start .info-lbl-instances-status-stopped").addClass('info-lbl-instances-ext');
		}
	}

	function setStartStyleUnhover(elem) {
		if (!$(elem).hasClass('status-stopped-scale')) {
			$(".status-stopped-start .info-lbl-instances-status-stopped").removeClass('info-lbl-instances-ext');
			$(elem).removeClass('status-stopped-start').addClass('status-stopped');
			$(".tc-indicators-circle-status-stopped .info-quantity-instances>span").text(infoQty);
			$(".tc-indicators-circle-status-stopped .info-lbl-instances-status-stopped>span.lbl-status-stopped").text(infoLbl);
			infoQty = '';
			infoLbl = '';
		}
	}

	function scaleUp(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances++;

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + " .info-quantity-instances>span").text(instances);
			if (instances > 0) {
				$(elem + " ." + iconClass).removeClass("disabled");
				$(elem + " .info-lbl-instances-status-stopped").removeClass("disabled");
				$(elem + " .info-lbl-instances-ext").removeClass("disabled");
			}
			// add Scale class if needed
			addScaleClass(elem);
			// add Start class if needed
			addStartClass(elem);
			// add Start class if needed
			addScaleFromErrorClass(elem);
		} else {
			if (!$(elem + " ." + iconClassUp).hasClass("disabled")) {
				$(elem + " ." + iconClassUp).addClass("disabled");
			}
		}
		// Verify Caret on Scale Up
		verifyCaretOnScaleUp(elem, instances);
	}

	function scaleUpRunning(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances++;

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + " .info-quantity-instances>span").text(instances);
			if (instances > 0) {
				$(elem + " ." + iconClass).removeClass("disabled");
				$(elem + " .info-lbl-instances-status-running").removeClass("disabled");
				$(elem + " .info-lbl-instances-ext").removeClass("disabled");
			}
			// add Scale class if needed
			addScaleClass(elem);
			// add Start class if needed
			addStartClass(elem);
			// add Start class if needed
			addScaleFromErrorClass(elem);
		} else {
			if (!$(elem + " ." + iconClassUp).hasClass("disabled")) {
				$(elem + " ." + iconClassUp).addClass("disabled");
			}
		}
		// Verify Caret on Scale Up
		verifyCaretOnScaleUp(elem, instances);
	}

	function scaleUpError(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances++;

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + " .info-quantity-instances>span").text(instances);
			if (instances > 0) {
				$(elem + " ." + iconClass).removeClass("disabled");
				$(elem + " .info-lbl-instances-status-error").removeClass("disabled");
				$(elem + " .info-lbl-instances-ext").removeClass("disabled");
			}
			// add Scale class if needed
			addScaleClass(elem);
			// add Start class if needed
			addStartClass(elem);
			// add Start class if needed
			addScaleFromErrorClass(elem);
		} else {
			if (!$(elem + " ." + iconClassUp).hasClass("disabled")) {
				$(elem + " ." + iconClassUp).addClass("disabled");
			}
		}
		// Verify Caret on Scale Up
		verifyCaretOnScaleUp(elem, instances);
	}

	function scaleDown(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances--;

		if (instances <= 0) {
			removeScaleClass(elem);
			removeStartClass(elem);
			removeScaleFromErrorClass(elem);
			$(elem + " ." + iconClass).addClass("disabled");
			$(elem + " .info-lbl-instances-status-stopped").addClass("disabled");
			$(elem + " .info-lbl-instances-ext").addClass("disabled");
		} else {
			$(elem + " ." + iconClass).removeClass("disabled");
			$(elem + " .info-lbl-instances-status-stopped").removeClass("disabled");
		}

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + "  .info-quantity-instances>span").text(instances);
			$(elem + " ." + iconClassUp).removeClass("disabled");
		} else {
			$(elem + "  .info-quantity-instances>span").text('0');
		}
		// Verify Caret on Scale Down
		verifyCaretOnScaleDown(elem, instances);
	}

	function scaleDownRunning(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances--;

		if (instances <= 0) {
			removeScaleClass(elem);
			removeStartClass(elem);
			removeScaleFromErrorClass(elem);
			$(elem + " ." + iconClass).addClass("disabled");
			$(elem + " .info-lbl-instances-status-running").addClass("disabled");
			$(elem + " .info-lbl-instances-ext").addClass("disabled");
		} else {
			$(elem + " ." + iconClass).removeClass("disabled");
			$(elem + " .info-lbl-instances-status-running").removeClass("disabled");
		}

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + "  .info-quantity-instances>span").text(instances);
			$(elem + " ." + iconClassUp).removeClass("disabled");
		} else {
			$(elem + "  .info-quantity-instances>span").text('0');
		}
		// Verify Caret on Scale Down
		verifyCaretOnScaleDown(elem, instances);
	}

	function scaleDownError(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances--;

		if (instances <= 0) {
			removeScaleClass(elem);
			removeStartClass(elem);
			removeScaleFromErrorClass(elem);
			$(elem + " ." + iconClass).addClass("disabled");
			$(elem + " .info-lbl-instances-status-error").addClass("disabled");
			$(elem + " .info-lbl-instances-ext").addClass("disabled");
		} else {
			$(elem + " ." + iconClass).removeClass("disabled");
			$(elem + " .info-lbl-instances-status-error").removeClass("disabled");
		}

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + "  .info-quantity-instances>span").text(instances);
			$(elem + " ." + iconClassUp).removeClass("disabled");
		} else {
			$(elem + "  .info-quantity-instances>span").text('0');
		}
		// Verify Caret on Scale Down
		verifyCaretOnScaleDown(elem, instances);
	}

	function getQtyInstances(elem) {
		var instances = $(elem + " .info-quantity-instances>span").text();
		var pieces = instances.split("\/");
		instances = pieces[0];
		instances = Number(instances);
		return instances;
	}

	function addScaleClass(elem) {
		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			$(elem + " .info-lbl-instances-status-running>span.lbl-status-running").text("Scale");
			$(elem + " .info-lbl-instances-status-running.info-lbl-instances-ext").addClass('to-running-scale');
		}
	}

	function removeScaleClass(elem) {
		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			$(elem + " .info-lbl-instances-status-running>span.lbl-status-running").text("Scale Instances");
			$(elem + " .info-lbl-instances-status-running.info-lbl-instances-ext").removeClass('to-running-scale');
		}
	}

	function addStartClass(elem) {
		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			$(elem + " .info-lbl-instances-status-stopped>span.lbl-status-stopped").text("Scale");
			$(elem + " .info-lbl-instances-status-stopped.info-lbl-instances-ext").addClass('to-stopped-scale');
		}
	}

	function removeStartClass(elem) {
		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			$(elem + " .info-lbl-instances-status-stopped>span.lbl-status-stopped").text("Start instances");
			$(elem + " .info-lbl-instances-status-stopped.info-lbl-instances-ext").removeClass('to-stopped-scale');
		}
	}

	function addScaleFromErrorClass(elem) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			$(elem + " .info-lbl-instances-status-error>span.lbl-status-error").text("Scale");
			$(elem + " .info-lbl-instances-status-error.info-lbl-instances-ext").addClass('to-error-scale');
		}
	}

	function removeScaleFromErrorClass(elem) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			$(elem + " .info-lbl-instances-status-error>span.lbl-status-error").text("Start instances");
			$(elem + " .info-lbl-instances-status-error.info-lbl-instances-ext").removeClass('to-error-scale');
		}
	}

	function verifyCaretOnScaleUp(elem, instances) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			if (instances == 3) {
				$(".tc-indicators-caret-up-status-error.tc-indicator-error-caret-up")
					.addClass("tc-indicator-disabled-caret-up");
			} else {
				$(".tc-indicators-caret-up-status-error.tc-indicator-error-caret-up")
					.removeClass("tc-indicator-disabled-caret-up");
			}
			$(".tc-indicators-caret-down-status-error.tc-indicator-error-caret-down")
				.removeClass("tc-indicator-disabled-caret-down");
		}

		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			if (instances == 3) {
				$(".tc-indicators-caret-up-status-running.tc-indicator-running-caret-up")
					.addClass("tc-indicator-disabled-caret-up");
			} else {
				$(".tc-indicators-caret-up-status-running.tc-indicator-running-caret-up")
					.removeClass("tc-indicator-disabled-caret-up");
			}
			$(".tc-indicators-caret-down-status-running.tc-indicator-running-caret-down")
				.removeClass("tc-indicator-disabled-caret-down");
		}

		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			if (instances == 3) {
				$(".tc-indicators-caret-up-status-stopped.tc-indicator-stopped-caret-up")
					.addClass("tc-indicator-disabled-caret-up");
			} else {
				$(".tc-indicators-caret-up-status-stopped.tc-indicator-stopped-caret-up")
					.removeClass("tc-indicator-disabled-caret-up");
			}
			$(".tc-indicators-caret-down-status-stopped.tc-indicator-stopped-caret-down")
				.removeClass("tc-indicator-disabled-caret-down");
		}
	}

	function verifyCaretOnScaleDown(elem, instances) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			if (instances == 0) {
				$(".tc-indicators-caret-down-status-error.tc-indicator-error-caret-down")
					.addClass("tc-indicator-disabled-caret-down");
			} else {
				$(".tc-indicators-caret-down-status-error.tc-indicator-error-caret-down")
					.removeClass("tc-indicator-disabled-caret-down");
			}
			$(".tc-indicators-caret-up-status-error.tc-indicator-error-caret-up")
				.removeClass("tc-indicator-disabled-caret-up");
		}

		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			if (instances == 0) {
				$(".tc-indicators-caret-down-status-running.tc-indicator-running-caret-down")
					.addClass("tc-indicator-disabled-caret-down");
			} else {
				$(".tc-indicators-caret-down-status-running.tc-indicator-running-caret-down")
					.removeClass("tc-indicator-disabled-caret-down");
			}
			$(".tc-indicators-caret-up-status-running.tc-indicator-running-caret-up")
				.removeClass("tc-indicator-disabled-caret-up");
		}

		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			if (instances == 0) {
				$(".tc-indicators-caret-down-status-stopped.tc-indicator-stopped-caret-down")
					.addClass("tc-indicator-disabled-caret-down");
			} else {
				$(".tc-indicators-caret-down-status-stopped.tc-indicator-stopped-caret-down")
					.removeClass("tc-indicator-disabled-caret-down");
			}
			$(".tc-indicators-caret-up-status-stopped.tc-indicator-stopped-caret-up")
				.removeClass("tc-indicator-disabled-caret-up");
		}
	}
});
$(function () {
	var open = true;
	$("#tcLeftMenuOptions").css("height", "140px");
	$(".tc-left-menu-toggle-icon").on("click", function () {
		if (open) {
			$(".tc-left-menu-toggle-icon").css("transform", "rotate(180deg)");
			$(".tc-left-menu-toggle-icon").css("-webkit-transform", "rotate(180deg)");
			$(".tc-left-menu-options-opacity").removeClass("tc-left-menu-options-opacity-off");
			$(".tc-left-menu-options").css("width", "0");
			$(".tc-left-menu-collapsible").css("width","275px");
			$(".tc-left-menu-options-opacity").addClass("tc-left-menu-options-opacity-on");
			$(".tc-left-menu-toggle-icon-container").css("float", "left");
		} else {
			$(".tc-left-menu-options-opacity").removeClass("tc-left-menu-options-opacity-on");
			$(".tc-left-menu-collapsible").css("width", "275px");
			$(".tc-left-menu-toggle-icon").css("transform", "rotate(0deg)");
			$(".tc-left-menu-toggle-icon").css("-webkit-transform", "rotate(0deg)");
			$(".tc-left-menu-options").css("width", "240px");
			$(".tc-left-menu-toggle-icon-container").css("float", "right");
			$(".tc-left-menu-options-opacity").addClass("tc-left-menu-options-opacity-off");
		}
	    open = !open;
	});
});

/**
 * Created by hede on 11/23/2017.
 */
// $(document).ready(function () {
//     console.log("Inside modals");
//     //debugger;
//     $(".modal-large-body").mCustomScrollbar({
//         scrollbarPosition: "inside"
//     });
// });
// /**
//  * Created by hede on 9/28/2017.
//  */
//
//     $(document).ready(function () {
//         console.log("Inside notification");
//     $(".tc-details-sent").mCustomScrollbar({
//         scrollbarPosition:"outside"
//     });
//
// });
//

/**
 * Created by hede on 9/29/2017.
 */

function matchCountry(element, input) {
    var data = eval(element.getAttribute("data-var"));
    var reg = new RegExp(input.split('').join('\\w*').replace(/\W/, ""), 'i');
    if (data) {
        return data.filter(function (country) {
            if (country.value.match(reg)) {
                return country;
            }
        });
    } else {
        return '';
    }
}

function changeInput(element, val) {
    var autoCompleteResult = matchCountry(element, val);
    var temp = '';
    autoCompleteResult.forEach(function (entry) {
        temp += "<li onclick='selectItem(this)'>" + entry.value + "</li>";
    });
    document.getElementById("result").innerHTML = temp;
    document.getElementById("result").style.display = 'block';
}

function selectItem(item) {
    document.querySelector('.tc-search .tc-search-container > input').value = item.textContent;
    document.getElementById("result").style.display = 'none';
}



$(document).ready(function () {
  //Initialize tooltips
  if($('.nav-tabs > li a[title]').length)
    $('.nav-tabs > li a[title]').tooltip();

  //Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

    var $target = $(e.target);

    if ($target.parent().hasClass('disabled')) {
      return false;
    }
  });

  $(".next-step").click(function (e) {

    var $active = $('.tc-wizard .nav-tabs li.active');
    $active.next().removeClass('disabled');
    nextTab($active);

  });
  $(".prev-step").click(function (e) {

    var $active = $('.tc-wizard .nav-tabs li.active');
    prevTab($active);

  });
});

function nextTab(elem) {
  $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
  $(elem).prev().find('a[data-toggle="tab"]').click();
}
$(function(){
	$(".tc-table>tbody>tr").on('click', function(){
		$(this).addClass('selected').siblings().removeClass('selected');
        if($(this).hasClass("checked")) {
            $(this).removeClass('selected');
        }
	});
});

$(function(){
    $(".tc-table>tbody>tr>td>div>input[type=checkbox-deleted]+label").click(function(){
        var mElem = $(this).parents("tr");
        if(!$(this).prev().is(":checked")) {
            mElem.removeClass('selected').addClass('checked');
        } else {
            mElem.removeClass('selected checked');
        }
    });
});

$(function(){
    $('input[type=checkbox-deleted].tc-table-checkbox-deleted-select-all').on('click',function(){
        if(this.checked){
            toggleCheck(true);
            $('.tc-table>tbody>tr').each(function(){
                $(this).removeClass('selected').addClass('checked');
            });
        }else{
            toggleCheck(false);
            $('.tc-table>tbody>tr').each(function(){
                $(this).removeClass('selected checked');
            });
        }
    });
    $('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]').on('click',function(){
        if($('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]:checked').length == $('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]').length){
            $('input[type=checkbox-deleted].tc-table-checkbox-deleted-select-all').prop('checked',true);
        }else{
            $('input[type=checkbox-deleted].tc-table-checkbox-deleted-select-all').prop('checked',false);
        }
    });
});

function toggleCheck(value) {
    $('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]').each(function(){
        this.checked = value;
    });
};
$(function () {
  if($('[data-toggle="tc-tooltips"]').length)
    $('[data-toggle="tc-tooltips"]').tooltip();
});

/**
 * Created by scheripa on 2/6/2018.
 */
/**
 * Created by hede on 10/13/2017.
 */

// $(document).ready(function () {
//     console.log("Inside dropdown new");
//     /* if($(".tc-dropdown > .dropdown-menu").length){
//      $(".tc-dropdown > .dropdown-menu").mCustomScrollbar({
//      scrollbarPosition:"inside"
//      });
//      }
//      */
//     $("body").click(function(ev){
//         console.log("clicked out");
//         if ($(".dropdown-selector").length) {
//             console.log("inside");
//             if (!ev.target.parentNode.matches('.dropdown-selector')) {
//                 /*  if (!($(ev).hasClass('.dropdown-selector'))) {*/
//                 console.log("target not match");
//                 var dropdowns = document.getElementsByClassName("dropdown-menu");
//                 var i;
//                 for (i = 0; i < dropdowns.length; i++) {
//                     var openDropdown = dropdowns[i];
//                     if (openDropdown.classList.contains('tc-dd-show')) {
//                         console.log("openDropdown if");
//                         openDropdown.classList.remove('tc-dd-show');
//                     }
//                 }
//             }
//         }
//     });
//
// });

document.querySelector('body').onclick = function (event) {
    if (event.target != undefined && !event.target.parentNode.matches('.tc-dropdown')
        && !event.target.parentNode.matches('.tc-buttons-dropdown')
        && !event.target.parentNode.matches('.dropdown-selector')
        && !event.target.parentNode.matches('.tc-search')
        && !event.target.parentNode.matches('.tc-search-container')) {
        showHideAllElements('.dropdown-menu, .tc-search-results', "none");
    }
};

// addClickEventListener(document.getElementsByTagName('body'),  );
function openDropDown(elem) {
    var dropDown = elem.nextElementSibling;
    if(dropDown.classList.contains('dropdown-menu')) {
        dropDown.style.display = "block";
    }
}

function showHideAllElements(element, displayType) {
    var elements = document.querySelectorAll(element);
    for (var index = 0; index < elements.length; index++) {
        elements[index].style.display = displayType;
    }
}



