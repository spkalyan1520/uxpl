###Version 1.1.0
  * This was released in February 2018.
  * UXPL is now available as a node module. The readme file explains how to include the private registry where UXPL is hosted and how to run npm install uxpl.
  * We’ve removed the dependency on Bootstrap and JQuery libraries.
  * We’re using a CI/CD pipeline to improve the release process.
  * Feedback from the Contact form now opens JIRA tickets and sends an email to the submitter. The email includes the ticket number, so you can track progress of your ticket.
  * A new Updates page keeps you informed about each release.