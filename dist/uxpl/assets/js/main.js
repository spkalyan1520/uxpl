!function ($) {
    'use strict';


    // Scrollspy
    // var $window = $(window)
    // var $body = $(document.body)
    // var offsetTop = 76;
    //
    // $body.scrollspy({
    //     target: '.uipsite-sidebar',
    //     scrollspy:scrollspy,
    //     offset: offsetTop
    // });
    // $window.on('load', function () {
    //     $body.scrollspy('refresh')
    // });
    var css_external = '';
    var js_external = '';

    var BASE_PATH = "../site/_config/head.json";
    var menu = $(".uipsite-sidenav");
    var menuItems = menu.find("a");
    var offset = $('header .navbar.navbar-default').height() + 30;
    // Anchors corresponding to menu items
    var scrollItems = menuItems.map(function () {
        var item = $($(this).attr("href"));
        if (item.length) {
            return item;
        }
    });
    $(window).load(function () {
        var href = window.location.hash;
        if (href != '') {
            activateMenuClick(href);
        }
    });
    var activateMenuClick = function (href) {
        var offsetTop = (href === "#") ? 0 : ($(href).offset().top - offset + 1);
        var currentId = href.replace('#', '');

        $(window).off("scroll", onSectionScroll);
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 300, function () {
            activateMenuItem(currentId);
            $(window).on("scroll", onSectionScroll);
        });
    }
    $(window).scroll(
        $.throttle(250, onSectionScroll)
    );

    function onSectionScroll() {
        // Get container scroll position
        //console.log($(this))
        var topDistance = $(this).scrollTop() + offset;
        // Get id of current scroll item
        var cur = scrollItems.map(function () {
            if ($(this).offset().top < topDistance)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";
        activateMenuItem(id);

    }

    function activateMenuItem(id) {
        // Set/remove active class
        menuItems
            .parent().removeClass("active")
            .end().filter("[href='#" + id + "']").parent().addClass("active");

        menuItems.filter("[href='#" + id + "']").parent().parent('ul').parent().addClass("active");
    }

    // for click,
    menuItems.click(function (e) {
        // e.preventDefault();
        var href = $(this).attr("href");
        activateMenuClick(href);

        // var offsetTop = (href === "#") ? 0 : ($(href).offset().top - offset + 1);
        // var currentId = href.replace('#', '');
        //
        // $(window).off("scroll", onSectionScroll);
        // $('html, body').stop().animate({
        //     scrollTop: offsetTop
        // }, 300, function () {
        //     activateMenuItem(currentId);
        //     $(window).on("scroll", onSectionScroll);
        // });
    });

    //===================
    // Highlight select menu
    $("ul.nav.navbar-nav").find('li').each(function (index, li) {
        var href = $(li).find('a').attr('href');
        var path = location.pathname;
        if (path.search(href) == -1) {
            $(li).removeClass('active');
        } else {
            $(li).addClass('active');
        }
    })


    //

    // Kill links
    //$('.bs-docs-container [href=#]').click(function (e) {
    //    e.preventDefault()
    //})

    // Sidenav affixing
    setTimeout(function () {
        var $sideBar = $('.uipsite-sidebar');

        $sideBar.affix({
            offset: {
                top: function () {
                    var offsetTop = $sideBar.offset().top;

                    var sideBarMargin = parseInt($sideBar.children(0).css('margin-top'), 10);

                    var navOuterHeight = $('header .navbar.navbar-default').height();
                    // $("#myBlock").css("marginBottom");
                    // var pageHeaderMargin = $('page-header').
                    return (this.top = offsetTop - navOuterHeight - sideBarMargin);
                },
                bottom: function () {
                    return (this.bottom = $('.uipsite-footer').outerHeight(true));
                }
            }
        })
    }, 100);

    setTimeout(function () {
        $('.bs-top').affix();
    }, 100);


    hljs.initHighlightingOnLoad();

    $('[data-toggle="tooltip"]').tooltip();

    // Click code to copy code
    function UIPatternComponentsSnippets() {
        'use strict';

        // Find all code snippets.
        this.snippets = document.querySelectorAll('code.markup');
        this.init();
    }

    /**
     * Initializes the UIPatternComponentsSnippets components.
     */
    UIPatternComponentsSnippets.prototype.init = function () {
        'use strict';

        [].slice.call(this.snippets).forEach(function (snippet) {
            snippet.addEventListener('click', this.onMouseClickHandler(snippet));
            snippet.addEventListener('mouseout', this.onMouseOutHandler(snippet));
        }, this);
        this.getHeadData();

    };
    UIPatternComponentsSnippets.prototype.getHeadData = function () {
        var headJsonPath = {
            "styles": [
                "/vendor/uxpl/css/tibco-cloud-ui-pattern.css",
                "/assets/css/main.css",
            ],
            "scripts": [
                "/vendor/uxpl/js/tibco-cloud-ui-pattern.js",
                "/assets/js/main.js",
            ],
            "mixins_variables": ["https://gitlab.com/spkalyan1520/uxpl/raw/master/src/components-src/mixins.less", "https://gitlab.com/spkalyan1520/uxpl/raw/master/dist/uxpl/vendor/uxpl/css/variables.less"]
        };


        var HOSTED_RESOURCE_PATH = "https://gitlab.com/spkalyan1520/uxpl/raw/master/dist";

        var resourcesArray = [];
        $.each(headJsonPath.scripts, function (key, value) {
            js_external = js_external + HOSTED_RESOURCE_PATH + value.trim();
            // resourcesArray.push('<script src="' + HOSTED_RESOURCE_PATH + value + '"></script>');
            if (key != headJsonPath.scripts.length - 1) {
                js_external = js_external + ';';
            }
        });
        $.each(headJsonPath.styles, function (key, value) {
            css_external = css_external + HOSTED_RESOURCE_PATH + value.trim();
            if (key != headJsonPath.styles.length - 1) {
                css_external = css_external + ';';
            }
            // resourcesArray.push('<link rel="stylesheet" href="' + HOSTED_RESOURCE_PATH + value + '">');
        });

        $.each(headJsonPath.mixins_variables, function (key, value) {
            if (css_external != '' && key == 0) {
                css_external = css_external + ";"
            }

            css_external = css_external + value.trim();
            if (key != headJsonPath.mixins_variables.length - 1) {
                css_external = css_external + ';';
            }

        });

        CodeBlockCodePen.prototype.UIPLIBS = resourcesArray;

    }
    /**
     * Store strings for class names defined by this component that are used in
     * JavaScript. This allows us to simply change it in one place should we
     * decide to modify at a later date.
     * @enum {string}
     * @private
     */
    UIPatternComponentsSnippets.prototype.CssClasses_ = {
        COPIED: 'copied',
        NOT_SUPPORTED: 'nosupport'
    };

    /**
     * Copies content of a <code> element into the system clipboard.
     * Not all browsers may be supported. See the following for details:
     * http://caniuse.com/clipboard
     * https://developers.google.com/web/updates/2015/04/cut-and-copy-commands
     * @param  {HTMLElement} snippet The <code> element containing the snippet code
     * @return {bool} whether the copy operation is succeeded
     */
    UIPatternComponentsSnippets.prototype.copyToClipboard = function (snippet) {
        'use strict';

        var sel = window.getSelection();
        var snipRange = document.createRange();
        snipRange.selectNodeContents(snippet);
        sel.removeAllRanges();
        sel.addRange(snipRange);
        var res = false;
        try {
            res = document.execCommand('copy');
        } catch (err) {
            // copy command is not available
            console.error(err);
        }
        sel.removeAllRanges();
        return res;
    };

    /**
     * Returns a mouseClickHandler for a snippet <code> element.
     * @param  {HTMLElement} snippet The <code> element containing the snippet code
     * @return {function} the click handler
     */
    UIPatternComponentsSnippets.prototype.onMouseClickHandler = function (snippet) {
        'use strict';

        return function () {
            if (window.getSelection().toString().length > 0) {
                // user has selected some text manually
                // don't do anything
                return;
            }
            var cls = this.CssClasses_.COPIED;
            if (!this.copyToClipboard(snippet)) {
                cls = this.CssClasses_.NOT_SUPPORTED;
            }
            snippet.classList.add(cls);
        }.bind(this);
    };

    /**
     * Returns a mouseOutHandler for a snippet <code> element.
     * @param  {HTMLElement} snippet The <code> element containing the snippet code
     * @return {function} the click handler
     */
    UIPatternComponentsSnippets.prototype.onMouseOutHandler = function (snippet) {
        'use strict';

        return function () {
            snippet.classList.remove(this.CssClasses_.COPIED);
        }.bind(this);
    };

    window.addEventListener('load', function () {
        'use strict';

        new UIPatternComponentsSnippets();
    });

    // click codepen
    function CodeBlockCodePen() {
        'use strict';

        this.codepenButtons = document.getElementsByClassName('codepen-button');
        this.init();
    }

// Also insert the UI Pattern Library.
//     CodeBlockCodePen.prototype.UIPLIBS = [
//         '<!-- UI Pattern -->',
//         '<script src="http://localhost:5000/vendor/jquery/jquery.js"></script>',
//         '<script src="http://localhost:5000/vendor/bootstrap/js/bootstrap.min.js"></script>',
//         '<script src="http://localhost:5000/vendor/uxpl/js/tibco-cloud-ui-pattern.js"></script>',
//         '<link rel="stylesheet" href="https://gitlab.com/spkalyan1520/uxpl/raw/master/dist/vendor/bootstrap/css/bootstrap.min.css">',
//         '<link rel="stylesheet" href="http://localhost:5000/vendor/uxpl/css/tibco-cloud-ui-pattern.css">',
//     ];

    /**
     * Creates CodePen buttons in all code blocks (`pre`) that are HTML.
     */
    CodeBlockCodePen.prototype.init = function () {
        'use strict';

        [].slice.call(this.codepenButtons).forEach(function (form) {
            // Attach the click event to the codepen button.
            form.addEventListener('click', this.clickHandler(form, form.parentNode));
        }, this);

    };

    /**
     * Extracts the parts of the text that is inside occurrences of the tag and
     * endTag.
     * @param  {String} tag The start tag which content we need to extract
     * @param  {String} endTag The end tag which content we need to extract
     * @param  {String} text The text for which we need to extract the content in
     *                       the given tags
     * @return {Object} An Object with 2 attributes: textRemainder which contains
     *                  the text not inside any of the given tag. and tagContent
     *                  which contains a concatenation of what was inside the tags
     */
    CodeBlockCodePen.prototype.extractTagsContent = function (tag, endTag, text) {
        'use strict';
        var tagStartIndex;
        var tagEndIndex;
        var tagText = '';

        while (text.indexOf(tag) !== -1) {
            tagStartIndex = text.indexOf(tag);
            tagEndIndex = text.indexOf(endTag);
            tagText += text.substring(tagStartIndex + tag.length, tagEndIndex);
            text = text.substring(0, tagStartIndex).trim() + '\n' +
                text.substr(tagEndIndex + endTag.length).trim();
        }

        return {textRemainder: text, tagContent: tagText};
    };

    /**
     * Click handler for CodePen buttons. Prepares the content for CodePen and
     * submits the form.
     * @param  {HTMLElement} form The CodePen form
     * @param  {HTMLElement} pre The pre containing the code to send to CodePen
     * @return {function} The click handler
     */
    CodeBlockCodePen.prototype.clickHandler = function (form, pre) {
        'use strict';

        return function () {

            // Track codepen button clicks
            if (typeof ga !== 'undefined') {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'codepen',
                    eventAction: 'click',
                    eventLabel: window.location.pathname +
                    (window.location.hash ? window.location.hash : '')
                });
            }
            // Modify relative URLs to make them absolute.
            var code = pre.textContent.replace('../assets/demos/',
                window.location.origin + '/assets/demos/');
            // Extract <style> blocks from the source code.
            var cssExtractResult = this.extractTagsContent('<style>', '</style>',
                code);

            code = cssExtractResult.textRemainder;
            var css = cssExtractResult.tagContent.trim();
            var less = $(pre).find('.less').val();

            // Extract <script> blocks from the source code.
            var jsExtractResult = this.extractTagsContent('<script>', '</script>',
                code);

            code = jsExtractResult.textRemainder.trim();
            var js = jsExtractResult.tagContent.trim();

            // Remove <input> children from previous clicks.
            while (form.firstChild) {
                form.removeChild(form.firstChild);
            }
            var input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'data');
            input.setAttribute('value', JSON.stringify(
                {
                    title: 'UI Pattern Component Demo',
                    html: '<html>\n' +
                    // '  <head>\n    ' + this.UIPLIBS.join('\n    ') + '\n  </head>\n' +
                    '  <body>\n    ' + code.split('\n').join('\n    ') + '\n  </body>\n' +
                    '</html>',
                    css: less,
                    css_pre_processor: 'less',
                    css_external: css_external,
                    js_external: js_external


                }))
            ;
            form.appendChild(input);

            form.submit();
        }.bind(this);
    };


    window.addEventListener('load', function () {
        'use strict';

        new CodeBlockCodePen();
    });

    $("#sendMail").click(function (event) {
        // validation comes here
        if( !$("#nameField").val() ) {
            console.log("Name is wrong");
            // $('#fileUploadForm input').addClass("errorBorder");
            $('#nameField').css('border-bottom', "1px solid red");
            $('#nameField ~ .tc-suggest').css('display','block');
            $('#nameField ~ .tc-suggest').addClass('contact-form-input-error');
            setTimeout(function() { $('#nameField').focus(); }, 50);
            $('html, body').animate({
                scrollTop: ($("#nameField").offset().top - 60)
            }, 500);
            return
        }
        if( !$("#emailField").val() ) {
            console.log("email is wrong");
            // $('#fileUploadForm input').addClass("errorBorder");
            $('#emailField').css('border-bottom', "1px solid red");
            $('#emailField ~ .tc-suggest').addClass('contact-form-input-error');
            setTimeout(function() { $('#emailField').focus(); }, 50);
            $('html, body').animate({
                scrollTop: ($("#emailField").offset().top - 60)
            }, 500);
            return
        }
        if( !$("#areaField").val() ) {
            console.log("Search is wrong");
            // $('#fileUploadForm input').addClass("errorBorder");
            $('#areaField').css('border-bottom', "1px solid red");
            $('#areaField ~ .tc-suggest').html('Select an area');

            $('#areaField ~ .tc-suggest').addClass('contact-form-input-error');
            $('#areaField').addClass("error");
            setTimeout(function() { $('#areaField').focus(); }, 50);
            $('html, body').animate({
                scrollTop: ($("#areaField").offset().top-60)
            }, 500);
            return
        }
        if( $("#areaField").val() ) {
            $('#areaField').css('border-bottom',  "1px solid #727272");

                $('#areaField ~ .tc-suggest').html('What apsect of the UXPL do you need help with?');
            $('#areaField ~ .tc-suggest').removeClass('contact-form-input-error');
        }

        if( !$("#comment").val() ) {
            console.log("no comment");
            // $('#fileUploadForm input').addClass("errorBorder");
            $('#comment').css('border', "1px solid red");
            $('#comment ~ .tc-suggest').css('display','block');
            $('#comment ~ .tc-suggest').addClass('contact-form-input-error');

            setTimeout(function() { $('#comment').focus(); }, 50);
            $('html, body').animate({
                scrollTop: ($("#comment").offset().top - 60)
            }, 500);
            return
        }



if($("input").hasClass("contact-form-input-error")){
            console.log("One of the fields are not correct");
    $('html, body').animate({
        scrollTop: ($("#nameField").offset().top)
    }, 500);

    return

}

        //stop submit the form, we will post it manually.
        event.preventDefault();

        $('.parentLoader').show();

        // Get form
        var form = $('#fileUploadForm')[0];

        // Create an FormData object
        var data = new FormData(form);

        // If you want to add an extra field for the FormData
        //data.append("CustomField", "This is some extra data, testing");

        // disabled the submit button
        $("#sendMail").prop("disabled", true);
        console.log("Inside mail ajax n data is " + data);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/sayHello",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                // $("#result").text(data);
                console.log("SUCCESS : ", data);
                //alert(data.status);
                // $('#mail-notification').css('visibility', 'visible');
                $("#fileUploadForm input").prop("disabled", false);
                $("#comment").prop("disabled", false);
                // $("body").css("opacity", "1");
                // $("body").css("background-color", "rgba(255,255,255,1)");
                $('.parentLoader').hide();
                //  $( "#mail-notification" ).scrollTop( 0 );
                $("html, body").animate({
                    scrollTop: 0
                }, 500);
                $("#mail-notification-success .tc-notifications-message").text("Thank you! Your message has been sent.");
                $('#mail-notification-success').show();
                $('#mail-notification-success').delay(30000).fadeOut();


                // $('#mail-notification .tc-notifications-message').show("slide", { direction: "right" }, 1000);
                console.log("After show call");
                $("#sendMail").prop("disabled", false);
                $('.filecount').html("0");
                form.reset();
                $("#fileList").empty();

            },
            error: function (e) {

                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
                // alert("Error occured");
                // $('#mail-notification').css('visibility', 'visible');
                $('.parentLoader').hide();
                //  $( "#mail-notification" ).scrollTop( 0 );
                $("html, body").animate({
                    scrollTop: 0
                }, 500);
                $("#mail-notification-failed .tc-notifications-message").text("Error occured");
                $('#mail-notification-failed').show();
                $('#mail-notification-failed').delay(30000).fadeOut();


                /* $('#mail-notification .tc-notifications-message').show("slide", { direction: "right" }, 1000);*/
                $("#sendMail").prop("disabled", false);
                $('.filecount').html("0");
                form.reset();
                $("#fileList").empty();
            }
        });

    });
//--------------------------------------------------Ajax call Ends------------------------------
    $("#mail-notification-success .tc-success-close").click(function () {
        $('#mail-notification-success').hide();
    });
    $("#mail-notification-failed .tc-error-close").click(function () {
        $('#mail-notification-failed').hide();
    });

    $(".tc-refresh").click(function () {
        $('#fileUploadForm')[0].reset();
        $('.filecount').html("0");
        $('#fileList').empty();
    });
    /*$(".removeFile").click(function() {
     console.log("Inside remove file");
     $(this).parent('div').css( "background", "yellow" );
     });*/


    $("#fileList").on("click", ".removeFile", function (event) {
var x = $(".filecount").text();

        var y = parseInt(x);
if(y!= 0){y--}

        console.log( "Remove class is clicked x is "+x+" and y is "+y );
        $(this).parent('div').remove();
        //$(".filecount").html(y.toString());
        $(".filecount").text(y.toString());

    });

    /*$('#fileUploadForm input').blur(function()
    {
        if( !$(this).val() ) {
            console.log("Something is wrong");
           // $('#fileUploadForm input').addClass("errorBorder");
            $('#fileUploadForm input').css('border-bottom', "1px solid red !important" );
            $("#sendMail").prop("disabled",true);


        }
        else{
            $('#fileUploadForm input').css('border-bottom', "1px solid #727272" );
            $("#sendMail").prop("disabled",false);


        }
    });*/
    $('#nameField').blur(function()
    {
        if( !$(this).val() ) {
            console.log("on name blur");
            $('#nameField').css('border-bottom', "1px solid red" );
            $('#nameField ~ .tc-suggest').css('display','block');
            $('#nameField ~ .tc-suggest').addClass('contact-form-input-error');
        }
        else{
            $('#nameField').css('border-bottom', "1px solid #727272" );
            $('#nameField ~ .tc-suggest').css('display','none');
            $('#nameField ~ .tc-suggest').removeClass('contact-form-input-error');
        }
    });

    $('#emailField').blur(function()
    {//empty condition check?
        var emailaddress= $('#emailField').val();

        if (validateEmail(emailaddress)) {
            console.log('Nice!! your Email is valid, now you can continue..');
            $('#emailField').css('border-bottom', "1px solid #727272" );
            $('#emailField ~ .tc-suggest').removeClass('contact-form-input-error');
        }
        else {
            console.log('Invalid Email Address');
            $('#emailField').css('border-bottom', "1px solid red" );

            $('#emailField ~ .tc-suggest').addClass('contact-form-input-error');
        }
    });
    $('#comment').blur(function()
    {
        if( !$(this).val() ) {
            console.log("on comment blur");
            // $('#fileUploadForm input').addClass("errorBorder");
            $('#comment').css('border', "1px solid red" );
            $('#comment ~ .tc-suggest').css('display','block');

            $('#comment ~ .tc-suggest').addClass('contact-form-input-error');
            // $("#sendMail").prop("disabled",true);


        }
        else{
            $('#comment').css('border', "1px solid #727272" );
            //  $( "#nameField" ).next( ".tc-suggest" ).css( "color", "green" );
            $('#comment ~ .tc-suggest').css('display','none');
            $('#comment ~ .tc-suggest').removeClass('contact-form-input-error');
            // $("#sendMail").prop("disabled",false);


        }
    });



    /*$('.contact-form-search').blur(function()
    {
        if( !$(this).val() ) {

            console.log("on area blur+"+$('#areaField').val());
            // $('#fileUploadForm input').addClass("errorBorder");
            $('#areaField').css('border-bottom', "1px solid red" );
            $('#areaField ~ .tc-suggest').addClass('contact-form-input-error');
            // $("#sendMail").prop("disabled",true);


        }
        else{
            $('#areaField').css('border-bottom', "1px solid #727272" );
            $('#areaField ~ .tc-suggest').removeClass('contact-form-input-error');
            // $("#sendMail").prop("disabled",false);


        }
    });
*/
   function validateEmail(sEmail) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }

    $('#result li').on('click', function () {
        $('.contact-search-text').val(this.textContent);
        $('.contact-form-search label').css({"top": "2px", "font-size": "13px"});
    });
}(jQuery);