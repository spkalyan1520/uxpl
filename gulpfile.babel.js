/**
 */

'use strict';

// Include Gulp & Tools
import fs from 'fs';
import path from 'path';
import del from 'del';
import cp from "child_process";
import globby from 'globby';
import mustache from 'gulp-mustache';
import Q from 'q';
import less from 'less';
import _ from 'lodash';
import marked from 'marked';
import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import zip from 'gulp-zip';
import inject from 'gulp-inject';
const $ = gulpLoadPlugins();

let hostedLibsUrlPrefix = 'http://ux.tibco.com';

const reload = browserSync.reload;

const CONFIG = {
    root: 'client/ui-pattern-src',
    source: 'src',
    componentsSrc: 'src/components-src',
    layoutSrc: 'src/layouts-src',
    site: 'site',
    contributors: 'src/contributors',
    dest: 'dist/uxpl',
    files: 'dist/files',
    destUIP: 'vendor',
    bowerPath: 'bower_components',
    temp: '.temp',
    demo: 'demo'
};

const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

function getPreConfigData() {
    let pageData = {};

    //################################
    // Get pre config data
    let configDataPath = path.join(CONFIG.site, '_config', 'config.json');
    let configData = getJSONByPath(configDataPath);

    let headDataPath = path.join(CONFIG.site, '_config', 'head.json');
    let headData = getJSONByPath(headDataPath);

    let headerDataPath = path.join(CONFIG.site, '_config', 'header.json');
    let headerData = getJSONByPath(headerDataPath);

    pageData = _.merge({}, configData, headData, headerData);

    return pageData;
}

/**
 * Get JSON object by path.
 */
function getJSONByPath(jsonPath) {
    try {
        fs.accessSync(jsonPath);
        let jsonStr = fs.readFileSync(jsonPath, 'utf8');
        let jsonObj = JSON.parse(jsonStr);
        return jsonObj;
    } catch (err) {
        return {};
    }
}

function exist(pathToCheck) {
    try {
        fs.accessSync(pathToCheck);
        return true;
    } catch (err) {
        return false;
    }
}

function mkdir(dirPath) {
    if (!exist(dirPath)) {
        fs.mkdirSync(dirPath);
    }
}

function appendToLine(index, line, title, mainIndex, subIndex) {
    return line.substring(0, index) + '(id=\'' + getId(title, mainIndex, subIndex).trim() + '\')' + line.substring(index);
}

function getId(title, mainIndex, subIndex) {
    return parseInt(mainIndex + 1) + (subIndex ? '-' + subIndex : '') + '-' + (title.replace('\'', '').replace('"', '').replace(/[^\w]/gi, ''));
}
//============================================
// clean folders
gulp.task('clean', () => {
    // clean generate folder
    return del.sync([CONFIG.dest, CONFIG.temp, CONFIG.site + '/_data', CONFIG.componentsSrc + '/tibco-cloud-ui-pattern.less'], {force: true});
});

//============================================
// Style
gulp.task('styles:template', () => {
    // Delete before's
    var paths = globby.sync(["**/*.less", '!mixins.less', '!variables.less', '!tibco-cloud-ui-pattern.less'], {cwd: CONFIG.componentsSrc});
    return gulp.src(CONFIG.componentsSrc + '/tibco-cloud-ui-pattern.mustache')
        .pipe(mustache({lessfiles: paths}, {
            extension: '.less'
        }))
        .pipe(gulp.dest(CONFIG.componentsSrc));
});

gulp.task("styles:less", ["styles:template"], () => {

    return gulp.src([CONFIG.componentsSrc + '/tibco-cloud-ui-pattern.less'])
        .pipe($.less())
        .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
        .pipe(gulp.dest(CONFIG.temp + '/vendor/uxpl/css'))
        .pipe(gulp.dest(CONFIG.temp + '/package/uxpl/css'));
});
gulp.task("styles", ["styles:less"]);

//============================================
// Copy
gulp.task("copy:uip", () => {
    return gulp.src(["img/**/*"], {
        cwd: CONFIG.componentsSrc
    })
        .pipe(gulp.dest(CONFIG.temp + '/vendor/uxpl/img'))
        .pipe(gulp.dest(CONFIG.temp + '/package/uxpl/img'));
});

gulp.task("copy:fonts", () => {
    return gulp.src(["fonts/**/*"], {
        cwd: CONFIG.source
    })
        .pipe(gulp.dest(CONFIG.temp + '/vendor/uxpl/fonts'))
        .pipe(gulp.dest(CONFIG.temp + '/package/uxpl/fonts'));
});

gulp.task("copy:templates", () => {
    return gulp.src(['**/*'], {cwd: CONFIG.layoutSrc})
        .pipe(gulp.dest(CONFIG.temp + '/template'));
});

gulp.task('copy:site', ["copy:uip", "copy:fonts", "copy:templates", "copy:demo", "copy:notes", "copy:package_json"], () => {
    return gulp.src(['**/*', '!**/_*/', '!**/_*/**/*', '!**/*.jade', '!**/*.less', '!**/less'], {cwd: CONFIG.site})
        .pipe($.if(/\.js/i, $.replace('$$hosted_libs_prefix$$', hostedLibsUrlPrefix)))
        .pipe(gulp.dest(CONFIG.temp));
});

gulp.task('copy', ["copy:site"], () => {
    return gulp.src(['**/*', '!**/*.jade'], {cwd: CONFIG.temp})
        .pipe(gulp.dest(CONFIG.dest));
});

gulp.task('copy:demo', () => {
    return gulp.src(['**/*'], {cwd: CONFIG.demo})
        .pipe(gulp.dest(CONFIG.temp + '/demo'));
});

gulp.task('copy:notes', () => {
    return gulp.src(['README.md'], {cwd: CONFIG.site})
        .pipe(gulp.dest(CONFIG.temp + '/vendor/uxpl'))
        .pipe(gulp.dest(CONFIG.temp + '/package/uxpl'));
});

gulp.task('copy:package_json', () => {
    let json = getJSONByPath('./package.json');
    let packageJson = {
        name: json.name,
        version: json.version,
        description: json.description,
        author: json.author
    };
    fs.writeFileSync(CONFIG.temp + '/package/uxpl/package.json', JSON.stringify(packageJson, null, 2));
    return this;
});

gulp.task("variablesReplace", () => {
    return gulp.src(['**/variables.less'], {cwd: CONFIG.componentsSrc})
        .pipe($.replace('/vendor/uxpl/img/', 'https://gitlab.com/spkalyan1520/uxpl/raw/master/dist/vendor/uxpl/img/'))
        .pipe(gulp.dest(CONFIG.dest + "/vendor/uxpl/css"));
});
gulp.task('createZip', () => {
    // return gulp.src('**', {dot: true, cwd:ath.join(CONFIG.dest, 'vendor', '**', '*')})
    return gulp.src('**', {dot: true, cwd: path.join(CONFIG.dest, 'vendor/uxpl', '**', '*')})
        .pipe(zip('uxpl.zip'))
        .pipe(gulp.dest(CONFIG.temp));
});
gulp.task("copy:html", () => {
    console.log("Dest path", exist(CONFIG.dest + "components/index.html"));
    return gulp.src("/components/index.html", {cwd: CONFIG.dest})
        .pipe(gulp.dest(CONFIG.dest + '/vendor/uxpl/'));
});
gulp.task('copyZip', [ 'createZip'], () => {
    return gulp.src(CONFIG.temp + '/uxpl.zip')
        .pipe(gulp.dest(CONFIG.dest));
});

gulp.task('createUploadFolder', () => {
    return mkdir(CONFIG.files);
});

gulp.task("generate:notes", () => {
    let releaseNotes = getJSONByPath(path.join(CONFIG.site, "ReleaseNotes.json"));
    let content = [];
    let latestRelease = releaseNotes.releases && releaseNotes.releases[0];
    let notes = releaseNotes.releases && releaseNotes.releases[0] && releaseNotes.releases[0].notes;
    content.push("###" + latestRelease.version);
    notes = notes.map(function (line) {
        return "  * " + line;
    });
    content = content.concat(notes);
    fs.writeFileSync(path.join(CONFIG.site, "README.md"), content.join('\n'));
    return this;
});

gulp.task("generate:updates-content", () => {
    return gulp.src(path.join(CONFIG.site, "updates/index.jade"))
        .pipe(inject(gulp.src(path.join(CONFIG.site, "ReleaseNotes.json")), {
            starttag: '//- inject:updates',
            relative: true,
            transform: function (filePath, file) {
                // return file contents as string
                let fileContent = file.contents.toString('utf8');
                fileContent = JSON.parse(fileContent);
                let releases = fileContent.releases;
                let content = [];
                if(releases.length) {
                    releases.forEach((release, index) => {
                        if(release.version) {
                            if(index !== 0) {
                                content.push(Array(13).join(' ') + ".grey-separator-line");
                            }
                            content.push((index === 0 ? "" : Array(13).join(' ')) + "h1 " + release.version);
                        }
                        if(release.description) {
                            content.push(Array(13).join(' ') + "p " + release.description);
                        }
                        if(release.notes && release.notes.length) {
                            content.push(Array(13).join(' ') + "ul");
                            content = content.concat(release.notes.map(function (line) {
                                return Array(16).join(' ') + "li " + line;
                            }));
                        }
                    });
                }
                return content.join('\n');
            }
        }))
        .pipe(gulp.dest(path.join(CONFIG.site, "updates/")));
});

gulp.task("generate:release-notes", ["generate:notes", "generate:updates-content"]);


//============================================
// Javascript
gulp.task("javascript", () => {
    return gulp.src(["**/*.js"], {cwd: CONFIG.componentsSrc})
        .pipe($.concat('tibco-cloud-ui-pattern.js'))
        .pipe(gulp.dest(CONFIG.temp + "/vendor/uxpl/js"))
        .pipe(gulp.dest(CONFIG.temp + "/package/uxpl/js"));
});

//============================================
// Site - Compile Less files
gulp.task('site:less', () => {
    gulp.src(["assets/less/main.less"], {cwd: CONFIG.site})
        .pipe($.less())
        .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
        .pipe(gulp.dest(CONFIG.temp + '/assets/css'));
});

// Merge Header JSON with the site jade and generate html
gulp.task('site:jade', () => {
    return gulp.src(['**/*.jade', '!_layout/**/*', '!_includes/**/*'], {cwd: path.join(CONFIG.site, '_data')})
        .pipe($.data(function (file) {
            let dataPath = path.relative(CONFIG.site, file.path);
            let result = /^([^.]*)+\.jade$/.exec(dataPath);
            dataPath = result[1];
            var json = fs.readFileSync(CONFIG.site + "/" + dataPath + ".json", 'utf8');
            json = JSON.parse(json);
            return json;
        }))
        .pipe($.jade({
            pretty: true
        }))
        .pipe(gulp.dest(CONFIG.temp));

});

gulp.task('site', [], cb => {
    runSequence(
        ['site:less'],
        ['site:jade'],
        cb
    );
});

//============================================
// Generate header content for all pages
function getReadMe(dirPath) {

    try {
        // check whether `readme.md` file exist
        let readMePath = path.join(CONFIG.componentsSrc, dirPath, 'readme.md');
        fs.accessSync(readMePath);
        let readme = fs.readFileSync(readMePath, 'utf8');
        return marked(readme);
        //return readme;
    } catch (err) {
        return "";
    }
}

function getContributor(contributor) {
    path.join(CONFIG.contributors, contributor + '.json');
    try {
        // check whether `contributor.json` file exist
        let contributorJSONPath = path.join(CONFIG.contributors, contributor + '.json');
        fs.accessSync(contributorJSONPath);
        let contributorJSON = fs.readFileSync(contributorJSONPath, 'utf8');
        contributorJSON = JSON.parse(contributorJSON);
        return contributorJSON;
    } catch (err) {
        return null;
    }
}

function isDeleted(name) {

    let deletedIndex = name.lastIndexOf('-deleted');

    if (deletedIndex != -1 && name.length - 8 == deletedIndex) {
        return true;
    }
    return false;
}

gulp.task("generate:page-data:all", () => {
    // general preconfig for each page
    let preConfigPageData = getPreConfigData();
    // List of all pages to generate
    let pages = ['', 'design', 'copy', 'work', 'contact', 'updates'];
    pages.forEach((page) => {
        if (fs.existsSync(path.join(CONFIG.site, page, 'index.jade'))) {
            let content = {page: page, content: '', pageData: '', preConfigPageData: preConfigPageData};
            buildSideNav(content);
            mkdir(path.join(CONFIG.site, '_data', page));
            fs.writeFileSync(path.join(CONFIG.site, '_data', page, 'index.jade'), content.content);
            fs.writeFileSync(path.join(CONFIG.site, '_data', page, 'index.json'), JSON.stringify(content.pageData));
        }
    });
    return this;
});

function buildSideNav(content) {
    //Generate header data
    let pageDataPath = path.join(CONFIG.site, '_config', content.page, 'index.json');
    content.pageData = getJSONByPath(pageDataPath);
    content.pageData = _.merge({}, content.preConfigPageData, content.pageData);
    //Generate side nav data
    let indexPage = fs.readFileSync(path.join(CONFIG.site, content.page, 'index.jade'), 'utf8');
    let lines = indexPage ? indexPage.split('\n') : [];
    let sideNav = [];
    lines.forEach((line) => {
        let h1Index = line.indexOf('h1');
        let h2Index = line.indexOf('h2');
        if (h1Index != -1 && line.trim().indexOf('h1') == 0) {
            let title = line.substring(line.substring(h1Index + 1).indexOf(' ') + h1Index + 2);
            title = title.length < 30 ? title : title.substr(0, 27) + '...';
            line = appendToLine(h1Index + 2, line, title, sideNav.length);

            sideNav.push({title: title, id: getId(title, sideNav.length), components: []});
        } else if (h2Index != -1 && line.trim().indexOf('h2') == 0) {
            let title = line.substring(line.substring(h2Index + 1).indexOf(' ') + h2Index + 2);

            title = title.length < 30 ? title : title.substr(0, 27) + '...';
            if (sideNav.length == 0) {
                line = appendToLine(h2Index + 2, line, title, sideNav.length);
                sideNav.push({title: title, id: getId(title, sideNav.length), components: []});
            }
            else {
                line = appendToLine(h2Index + 2, line, title, sideNav.length - 1, sideNav[sideNav.length - 1].components.length);
                sideNav[sideNav.length - 1].components.push({
                    title: title,
                    id: getId(title, sideNav.length - 1, sideNav[sideNav.length - 1].components.length),
                    components: []
                });
            }
        }
        if (sideNav.length) //So that this category does not print in main page like the component categories
            sideNav[sideNav.length - 1].displayContent = false;
        content.content += line + '\n';
    });
    content.pageData.categories = sideNav;
}

gulp.task("copy:includes", () => {
    return gulp.src(['**/_includes/**/*', '**/_layout/**/*', '**/assets/**/*'], {cwd: CONFIG.site})
        .pipe(gulp.dest(path.join(CONFIG.site, '_data')));
});

//============================================
// Generate ** Components ** page data
gulp.task("generate:page-data:components", () => {
    const category = {
        "title": "Category Name",
        "path": "category",
        "description": "Category Description",
        "components": [],
        "displayContent": true
    };

    const component = {
        "path": "buttons/primary",
        "id": "category-buttons-primary",
        "title": "Buttons",
        "description": "",
        "contributors": {},
        "snippets": [],
        "less": ""
    };

    const SNIPPET = {
        "code": "",
        "demo": ""
    };

    let pageData = {
        "categories": {}
    };

    //################################
    // general preconfig for each page
    let preConfigPageData = getPreConfigData();

    //Generate getting started content then merge with the main components page
    let content = {page: 'components', content: '', pageData: '', preConfigPageData: preConfigPageData};
    buildSideNav(content);


    //################################
    // Generate categories
    let categories = [];
    let dir = fs.readdirSync(CONFIG.componentsSrc);
    for (let i = 0; i < dir.length; i++) {
        let dirPath = path.join(CONFIG.componentsSrc, dir[i]);
        // dir is a directory and it isn't `img` folder
        if (fs.lstatSync(dirPath).isDirectory() && dir[i] !== 'img' && dir[i] !== 'fonts') {
            //Do not display the deprecated components . Do not show any component that has a -deleted at the end of the name
            if (isDeleted(dirPath)) continue;

            // create an empty category
            let categoryClone = _.cloneDeep(category);

            categoryClone.title = _.startCase(dir[i]);
            categoryClone.path = dir[i];
            categoryClone.id = _.kebabCase(dir[i]);
            categoryClone.description = getReadMe(categoryClone.path);

            // generate ui components of this category

            let componentsDir = fs.readdirSync(path.join(CONFIG.componentsSrc, categoryClone.path));

            for (let j = 0; j < componentsDir.length; j++) {
                let componentPath = path.join(CONFIG.componentsSrc, categoryClone.path, componentsDir[j]);
                if (!fs.lstatSync(componentPath).isDirectory()) {
                    continue;
                }
                //Do not display the deprecated components . Do not show any component that has a -deleted at the end of the name
                if (isDeleted(componentsDir[j])) continue;
                let componentName = componentsDir[j].split('--');

                let compTitle = componentName[1] ? componentName[1] : componentName;
                let componentClone = _.cloneDeep(component);
                componentClone.title = _.startCase(compTitle);
                componentClone.path = path.join(categoryClone.path, componentsDir[j]);
                componentClone.id = _.kebabCase(componentClone.path);
                componentClone.description = getReadMe(componentClone.path);

                //Read the less in the components less files
                let componentDirList = fs.readdirSync(componentPath);
                // let mixinsLESS = fs.readFileSync(path.join(CONFIG.componentsSrc, 'mixins.less'));
                // let variablesLESS = fs.readFileSync(path.join(CONFIG.componentsSrc, 'variables.less'));
                for (let l = 0; l < componentDirList.length; l++) {
                    let regLESS = /(.)*\.less/;
                    if (!regLESS.test(componentDirList[l]))
                        continue;
                    //Combine variables.less, mixins.less and the less file
                    const fileContents = fs.readFileSync(path.join(componentPath, componentDirList[l]), 'utf8');
                    // const fileCombined = mixinsLESS+'\n'+variablesLESS+'\n'+fileContents;
                    // componentClone.less = fileCombined;
                    componentClone.less = fileContents;
                }

                let snippetsPath = path.join(CONFIG.componentsSrc, componentClone.path, 'snippets');

                let snippetsDir = fs.readdirSync(snippetsPath);

                // Read snippets
                for (let k = 0; k < snippetsDir.length; k++) {
                    let regHTML = /(.)*\.html/;
                    // If it isn't html, skip, current just support html
                    if (!regHTML.test(snippetsDir[k])) {
                        continue;
                    }
                    let snippet = fs.readFileSync(path.join(snippetsPath, snippetsDir[k]), 'utf8');

                    // Find Contributor Information
                    let result = snippet.match(/\<\!--\s*author\s*:\s*([a-zA-Z0-9]*)\s*--\>/gm);
                    for (let h = 0; h < (result && result.length) || 0; h++) {
                        let author = /\<\!--\s*author\s*:\s*([a-zA-Z0-9]*)\s*--\>/gm.exec(result[h])[1];
                        //componentClone.contributors[author]=getContributor(author);
                        if (!componentClone.contributors[author]) {
                            let contributorJson = getContributor(author);
                            if (contributorJson) {
                                componentClone.contributors[author] = contributorJson;
                            }
                        }
                    }

                    let snippetClone = _.cloneDeep(SNIPPET);
                    snippetClone.demo = snippet;

                    // If there is a snippet comment use that code to display the code window info
                    let snipp = snippet.match(/\<\!--\s*snippet\s*:/gm);
                    if (snipp && snipp.length) {
                        let snippetText = snippet.substring(snippet.indexOf('<!--snippet:') + 12, snippet.indexOf('snippet!-->'));
                        snippetClone.code = snippetText;
                    }
                    componentClone.snippets.push(snippetClone);
                }
                categoryClone.components.push(componentClone);
            }
            categories.push(categoryClone);
        }
    }
    //Add categories from the getting stared page 1st
    pageData.categories = content.pageData.categories;
    pageData.categories = pageData.categories.concat(categories);

    //################################
    // preconfig for components page
    let componentsPageDataPath = path.join(CONFIG.site, '_config', 'components', 'index.json');
    let componentsPageData = getJSONByPath(componentsPageDataPath);

    pageData = _.merge({}, preConfigPageData, pageData, componentsPageData);
    mkdir(path.join(CONFIG.site, '_data', 'components'));
    //Copy the new index file with the navigation id's populated
    fs.writeFileSync(path.join(CONFIG.site, '_data', content.page, 'index.jade'), content.content);
    fs.writeFileSync(path.join(CONFIG.site, '_data', 'components', 'index.json'), JSON.stringify(pageData));

    return this;
});


gulp.task('generate:page-data', ['generate:page-data:all', 'copy:includes', 'generate:page-data:components']);

//============================================
// Bower
gulp.task('bower', () => {

    // store all the promise
    let allPromises = [];

    // clean
    del.sync([CONFIG.bowerPath]);
    // bower install
    cp.execSync('bower install');

    // copy bootstrap
    // clean before version
    // del.sync([CONFIG.site + "/vendor/bootstrap"]);
    // let streamBootstrap = gulp.src(['bootstrap/dist/**/*', '!**/npm.js'], {cwd: CONFIG.bowerPath})
    //     .pipe(gulp.dest(CONFIG.site + "/vendor/bootstrap"));
    //
    // allPromises.push((function () {
    //     var deferred = Q.defer();
    //     streamBootstrap.on('end', () => {
    //         del.sync(["bootstrap"], {cwd: CONFIG.bowerPath});
    //         deferred.resolve("delete bootstrap successful");
    //     });
    //
    //     streamBootstrap.on('error', () => {
    //         deferred.resolve("delete bootstrap fail");
    //     });
    //
    //     return deferred.promise;
    // })());


    // copy jquery
    // clean before version
    del.sync([CONFIG.site + "/vendor/jquery"]);
    let streamJquery = gulp.src(['jquery/dist/**/*'], {cwd: CONFIG.bowerPath})
        .pipe(gulp.dest(CONFIG.site + "/vendor/jquery"));

    allPromises.push((function () {
        var deferred = Q.defer();
        streamJquery.on('end', () => {
            del.sync(["jquery"], {cwd: CONFIG.bowerPath});
            deferred.resolve("delete jquery successful");
        });

        streamJquery.on('error', () => {
            deferred.resolve("delete jquery fail");
        });

        return deferred.promise;
    })());

    // highlight
    // clean before version
    del.sync([CONFIG.site + "/vendor/highlightjs"]);
    let streamHighlightjs = gulp.src(['*.js', '**/styles/github.css'], {cwd: CONFIG.bowerPath + '/highlightjs'})
        .pipe(gulp.dest(CONFIG.site + "/vendor/highlightjs"));

    allPromises.push((function () {
        var deferred = Q.defer();
        streamHighlightjs.on('end', () => {
            del.sync(["highlightjs"], {cwd: CONFIG.bowerPath});
            deferred.resolve("delete highlightjs successful");
        });

        streamHighlightjs.on('error', () => {
            deferred.resolve("delete highlightjs fail");
        });

        return deferred.promise;
    })());

    // copy rest libraries to vendor
    Q.all(allPromises).then(() => {
        let stream = gulp.src(['**/*'], {cwd: CONFIG.bowerPath})
            .pipe(gulp.dest(CONFIG.site + "/vendor"));
        stream.on('end', () => {
            del.sync([CONFIG.bowerPath]);
            console.log("Delete bower folder successful");
        });

        stream.on('error', () => {
            console.log("Delete bower folder fail");
        });
    });
});

function watch() {
    console.log("=========watch: %s", new Date().getTime());
    gulp.watch([CONFIG.source + "/**/*"], ['runWatchChange']).on('change', logChange);
    gulp.watch([CONFIG.site + "/**/*", "!" + CONFIG.site + "/_data/**/*"], ['runWatchChange']).on('change', logChange);
}

let watchRunning = false;
gulp.task('runWatchChange', () => {
    if (watchRunning)
        return;
    watchRunning = true;
    runSequence(
        ['rebuild']
    );
});

gulp.task('rebuild', ['localdev'], () => {
    watchRunning = false;
});
function logChange(event) {
    if (watchRunning)
        return;
    let isSrcChange = event.path.indexOf(CONFIG.src) != -1;
    if (isSrcChange)
        console.log('Source Change. File ' + event.path + ' was ' + event.type + ', running task localdev');
    else
        console.log('Site Change. File ' + event.path + ' was ' + event.type + ', running tasks localdev');
    reload;
}
//============================================
// Tasks
gulp.task('default', ['clean', 'styles', 'javascript', 'copy']);

gulp.task('serve', ['localdev'], () => {
    $.connect.server({
        root: '.temp',
        port: 5000,
        livereload: true
    });
    watch();
});

// develop
gulp.task('development', ['clean'], cb => {
    runSequence(
        ['styles'],
        ['javascript'],
        ['generate:page-data'],
        ['site'],
        ['copy'],
        ['copyZip'],
        ['createUploadFolder'],
        cb
    );
});

gulp.task('localdev', ['clean'], cb => {
    hostedLibsUrlPrefix = "http://localhost:5000";
    runSequence(
        ['styles'],
        ['javascript'],
        ['generate:release-notes'],
        ['generate:page-data'],
        ['site'],
        ['copy'],
        ['copyZip'],
        ['createUploadFolder'],
        cb
    );
});

// production
gulp.task('production', ['clean'], cb => {
    runSequence(
        ['styles'],
        ['javascript'],
        ['generate:release-notes'],
        ['generate:page-data'],
        ['site'],
        ['copy'],
        ['copyZip'],
        ['createUploadFolder'],
        cb
    );
});

