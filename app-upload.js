//Configuration for production
//Use node app.js for prod, Use gulp serve for live reload in dev

var express = require('express');
var path = require('path');
var fileUpload = require('express-fileupload');

var app = express();

// Define the port to run on
app.set('port', 8080);
// default options
app.use(fileUpload({ safeFileNames: true }));

app.use(express.static(path.join(__dirname, 'dist')));
// Listen for requests
var server = app.listen(app.get('port'), function() {
    var port = server.address().port;
    console.log('Application started on port ' + port);
});

app.post('/feedback', function(req, res) {
    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    let uploadedFile = req.files.uploadedFile;

    // Use the mv() method to place the file somewhere on your server
    uploadedFile.mv('/attachments/'+uploadedFile.name, function(err) {
        if (err)
            return res.status(500).send(err);
        res.send('File uploaded!');
    });
});

var appendTimeStamp = function(name){
    var index = name.lastIndex('.');
    var ext = name.substring(index);
    var fileName = name.substring(0,index);
    fileName += Math.floor(new Date() / 1000);
    return fileName+ext;
}