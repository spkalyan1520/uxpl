## UI Pattern
1. Header (Ro)
2. Buttons(Daniel)
3. Background 
4. Input
5. Drop down
6. Dialog/modal box
7. Check box
8. Notification 
9. Tooltip
10. Text area
11. Table

This is come from VxD, APIM and MF

## Target to beta2
 1. Build the website
 2. Create at least one layout, 5 ui pattern
 3. Create an automation let user to contibute
 
## Target to Post-Beta2
1. Add more layout
2. Add more UI Pattern
3. Start to create reuse Angular Directive, Services

## Develop Plan
To develop this totally we need 3 weeks

1. First Week
  * Folder structure
  * Code standard
  * Automation  
2. Second Week
  * Implement Layout 
  * Implement UI Pattern
  * Document
  * Test
  * Refactor Tropos code to use this UI Pattern
3. Third Week
  * Implement Layout 
  * Implement UI Pattern
  * Document
  * Test
  * Refactor Tropos code to use this UI Pattern


