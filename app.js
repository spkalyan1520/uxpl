//Configuration for production
//Use node app.js for prod, Use gulp serve for live reload in dev

var express = require('express') ,
    path = require('path'), fs = require('fs'),
    /*bodyParser = require('body-parser'),*/ fileUpload = require('express-fileupload'),
    app = express(),
    router = express.Router(),
    request = require('request'),

    nodemailer = require('nodemailer'),
CryptoJS = require("crypto-js");



var config = require('./config/config');

var mailModule = require('./config/mail');
var jiraModule = require('./config/jiraService');

// Define the port to run on
app.set('port', 80);
app.get(/^(?!\/uxpl.*$).*/,(req, res)=>{
    res.redirect("/uxpl"+req.originalUrl);
});

/*app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));*/
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies

app.use(express.static(path.join(__dirname, 'dist')));
//app.use(express.bodyParser({uploadDir: './attachments'}));

var busboy = require('connect-busboy');
//...
app.use(busboy());
var str = config.username;

var keyarray = str.split("@");
var key = keyarray[0];

var decrypted = CryptoJS.AES.decrypt(config.password,key);

var  emailPwd = decrypted.toString(CryptoJS.enc.Utf8);
//console.log("Password "+emailPwd);



//Final email with attachment
var filedest ;
var files = [];
app.post('/sayHello',function(req,res){
    console.log("Inside say hello "+req.body+" "+config.username);
    var jiraNum ="";
    var name = "";
    var email ="";
    var area = "";
    var comment = "";
    var status = "";
    var fstream1;
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
        console.log("Uploading: " + filename);
        if(filename == ""){
            console.log("no file");
            return;
        }
        filedest = config.fileHome + Date.now()+'_'+filename;
        files.push(filedest);

        console.log("File is at before : " + filedest);

        fstream1 = fs.createWriteStream(filedest);
        file.pipe(fstream1);
        console.log("File is at after : " + filedest);
        /*fstream1.on('close', function () {
         res.redirect('back');
         });*/
    });
    req.busboy.on('field', function(key, value, keyTruncated, valueTruncated) {

        console.log("inside busboy key is "+key+" value is "+value);
        if(key == "email"){
            email = value;
            console.log("email val is "+value);

        }
        if(key == "area"){
            area = value;
            console.log("email val is "+area);

        }
        if(key == "comment"){
            comment = value;
            console.log("email val is "+comment);
        }
        if(key == "name"){
            name = value;
            console.log("email val is "+name);
        }

    });

    setTimeout(function(){ console.log("File uploaded");
    //jiraCall(files, email,name,comment,area,res);
        //uploadFiles(files);
       // handleSayHello(files,jiraNum, email,name,comment,area,res);
       // mailModule(files,jiraNum,email,name,comment,area,res);
        jiraModule(files, email,name,comment,area,res);
    }, 3000);
    console.log("After file upload");
     files = [];//emptying the files array
    jiraNum = "";
    console.log("Status is "+status);

});


var server = app.listen(app.get('port'), function() {
    var port = server.address().port;
    console.log('Application started on port ' + port);
});
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
