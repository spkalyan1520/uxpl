$(function () {
	var open = true;
	$("#tcLeftMenuOptions").css("height", "140px");
	$(".tc-left-menu-toggle-icon").on("click", function () {
		if (open) {
			$(".tc-left-menu-toggle-icon").css("transform", "rotate(180deg)");
			$(".tc-left-menu-toggle-icon").css("-webkit-transform", "rotate(180deg)");
			$(".tc-left-menu-options-opacity").removeClass("tc-left-menu-options-opacity-off");
			$(".tc-left-menu-options").css("width", "0");
			$(".tc-left-menu-collapsible").css("width","275px");
			$(".tc-left-menu-options-opacity").addClass("tc-left-menu-options-opacity-on");
			$(".tc-left-menu-toggle-icon-container").css("float", "left");
		} else {
			$(".tc-left-menu-options-opacity").removeClass("tc-left-menu-options-opacity-on");
			$(".tc-left-menu-collapsible").css("width", "275px");
			$(".tc-left-menu-toggle-icon").css("transform", "rotate(0deg)");
			$(".tc-left-menu-toggle-icon").css("-webkit-transform", "rotate(0deg)");
			$(".tc-left-menu-options").css("width", "240px");
			$(".tc-left-menu-toggle-icon-container").css("float", "right");
			$(".tc-left-menu-options-opacity").addClass("tc-left-menu-options-opacity-off");
		}
	    open = !open;
	});
});
