/**
 * @license
 * Copyright 2015-2016 TIBCO Inc. All Rights Reserved.
 */
$(function(){
	var infoQty = '';
	var infoLbl = '';
	var infoLblRunning = '';

	// Status Stopped
	/*$(".tc-indicators-circle-status-stopped").hover(function () {
		setStartStyleHover(this);
	}, function () {
		setStartStyleUnhover(this);
	});*/

	// Status Stopped - on click
	$(".tc-indicators-circle-status-stopped").on('click', function () {
		$(".tc-indicators-circle-status-stopped").removeClass("status-stopped-start");
		$(this).removeClass('status-stopped').addClass('status-stopped-scale');
		setStartInstancesStyle();
	});

	// tc-indicator-caret-up
	$(".tc-indicators-caret-up-status-stopped").on('click', function () {
		scaleUp(".tc-indicators-circle-status-stopped.status-stopped-scale", "tc-indicators-caret-down-status-stopped", "tc-indicators-caret-up-status-stopped");
	});

	// tc-indicator-caret-up
	$(".tc-indicators-caret-up-status-running").on('click', function () {
		scaleUp(".tc-indicators-circle-status-running.status-to-running-scale", "tc-indicators-caret-down-status-running", "tc-indicators-caret-up-status-running");
	});

	//.tc-indicators-error-caret-up
	$(".tc-indicators-caret-up-status-error").on('click', function () {
		scaleUp(".tc-indicators-circle-status-error.status-to-error-scale", "tc-indicators-caret-down-status-error", "tc-indicators-caret-up-status-error");
	});

	// tc-indicator-caret-down
	$(".tc-indicators-caret-down-status-stopped").on('click', function () {
		scaleDown(".tc-indicators-circle-status-stopped.status-stopped-scale", "tc-indicators-caret-down-status-stopped", "tc-indicators-caret-up-status-stopped");
	});

	// tc-indicator-caret-down
	$(".tc-indicators-caret-down-status-running").on('click', function () {
		scaleDown(".tc-indicators-circle-status-running.status-to-running-scale", "tc-indicators-caret-down-status-running", "tc-indicators-caret-up-status-running");
	});

	// tc-indicator-caret-down
	$(".tc-indicators-caret-down-status-error").on('click', function () {
		scaleDown(".tc-indicators-circle-status-error.status-to-error-scale", "tc-indicators-caret-down-status-error", "tc-indicators-caret-up-status-error");
	});

	// Status Running
	/*$(".tc-indicators-circle-status-running").hover(function () {
		if(!$(this).hasClass("status-to-running-scale")) {
			infoLblRunning = 
				infoLblRunning == '' 
					? $(".tc-indicators-circle-status-running .info-lbl-instances-status-running>span.lbl-status-running").text() : infoLblRunning;

			$(".tc-indicators-circle-status-running").addClass('status-running-scale');
			$(".tc-indicators-circle-status-running .info-lbl-instances-status-running>span.lbl-status-running").text('Scale instances');
			$(".tc-indicators-circle-status-running.status-running-scale .info-lbl-instances-status-running").addClass('info-lbl-instances-ext');
		}
	}, function () {

		if(!$(this).hasClass("status-to-running-scale")) {
			
			$(".tc-indicators-circle-status-running.status-running-scale").removeClass('status-running-scale');
			$(".info-lbl-instances-status-running").addClass('status-running');
			$(".tc-indicators-circle-status-running .info-lbl-instances-status-running").removeClass('info-lbl-instances-ext');
			$(".tc-indicators-circle-status-running .info-lbl-instances-status-running>span.lbl-status-running").text(infoLblRunning);
			
			infoLblRunning = '';
		}

	});*/

	// Status Error
	/*$(".tc-indicators-circle-status-error").hover(function () {
		if(!$(this).hasClass("status-to-error-scale")) {
			infoLblRunning = 
				infoLblRunning == '' 
					? $(".tc-indicators-circle-status-error .info-lbl-instances-status-error>span.lbl-status-error").text() : infoLblRunning;

			$(".tc-indicators-circle-status-error").addClass('status-error-scale');
			$(".tc-indicators-circle-status-error .info-lbl-instances-status-error>span.lbl-status-error").text('Scale instances');
			$(".tc-indicators-circle-status-error.status-to-error-scale .info-lbl-instances-status-error").addClass('info-lbl-instances-ext');
		}
	}, function () {

		if(!$(this).hasClass("status-to-error-scale")) {
			
			$(".tc-indicators-circle-status-error.status-error-scale").removeClass('status-error-scale');
			$(".info-lbl-instances-status-error").addClass('status-error');
			$(".tc-indicators-circle-status-error .info-lbl-instances-status-error").removeClass('info-lbl-instances-ext');
			$(".tc-indicators-circle-status-error .info-lbl-instances-status-error>span.lbl-status-error").text(infoLblRunning);
			
			infoLblRunning = '';
		}

	});*/

	// Status Running - on click
	$(".tc-indicators-circle-status-running").on('click', function () {
		$(this).removeClass('status-running');
		$(this).addClass("status-to-running-scale");
		
		$(".tc-indicators-circle-status-running.status-to-running-scale > .info-lbl-instances-status-running")
			.addClass('info-lbl-instances-ext');
		
		$(".tc-indicators-circle-status-running.status-to-running-scale > div.tc-indicators-caret-up-status-running")
			.addClass("tc-indicator-running-caret-up");
		
		$(".tc-indicators-circle-status-running.status-to-running-scale > div.tc-indicators-caret-down-status-running")
			.addClass("tc-indicator-running-caret-down");

		$(".tc-indicators-card > .tc-card-border-status-running").addClass("selected-running");

		$(".tc-indicators-card .tc-indicators-circle-status-running").addClass("tc-circle-no-border");

		var baseElem = ".tc-indicators-circle-status-running.status-to-running-scale";
		var instances = getQtyInstances(baseElem);

		if (instances > 0 && instances <= 3) {
			addScaleClass(".tc-indicators-circle-status-running.status-to-running-scale");
		} else {
			removeScaleClass(".tc-indicators-circle-status-running.status-to-running-scale");
		}
	});

	// Status Error - on click
	$(".tc-indicators-circle-status-error").on('click', function () {
		$(this).removeClass('status-error');
		$(this).addClass("status-to-error-scale");
		
		$(".tc-indicators-circle-status-error.status-to-error-scale > .info-lbl-instances-status-error")
			.addClass('info-lbl-instances-ext');
		
		$(".tc-indicators-circle-status-error.status-to-error-scale > div.tc-indicators-caret-up-status-error")
			.addClass("tc-indicator-error-caret-up");
		
		$(".tc-indicators-circle-status-error.status-to-error-scale > div.tc-indicators-caret-down-status-error")
			.addClass("tc-indicator-error-caret-down");

		$(".tc-indicators-card > .tc-card-border-status-error").addClass("selected-error");

		$(".tc-indicators-card .tc-indicators-circle-status-error").addClass("tc-circle-no-border");

		var baseElem = ".tc-indicators-circle-status-error.status-to-error-scale";
		var instances = getQtyInstances(baseElem);

		if (instances > 0 && instances <= 3) {
			addScaleFromErrorClass(".tc-indicators-circle-status-error.status-to-error-scale");
		} else {
			removeScaleFromErrorClass(".tc-indicators-circle-status-error.status-to-error-scale");
		}
	});

	$(".info-lbl-instances-status-running").on('click', function () {
		if ($(".tc-indicators-circle-status-running").hasClass("status-to-running-scale") && $(".info-lbl-instances-status-running").hasClass("to-running-scale")) {
			$(this).addClass("scaling");
			$(".lbl-status-running").addClass("hidden");
			$(".lbl-acting-status-running").removeAttr("hidden");
			$(".tc-indicators-caret-up-status-running").addClass("hidden");
			$(".tc-indicators-caret-down-status-running").addClass("hidden");
			$(".tc-indicators-circle-status-running.status-running-scale.status-to-running-scale").addClass("tc-running-spinner");
		}
	});

	$(".info-lbl-instances-status-error").on('click', function () {
		if ($(".tc-indicators-circle-status-error").hasClass("status-to-error-scale") && $(".info-lbl-instances-status-error").hasClass("to-error-scale")) {
			$(this).addClass("scaling");
			$(".lbl-status-error").addClass("hidden");
			$(".lbl-acting-status-error").removeAttr("hidden");
			$(".tc-indicators-caret-up-status-error").addClass("hidden");
			$(".tc-indicators-caret-down-status-error").addClass("hidden");
			$(".tc-indicators-circle-status-error.status-error-scale.status-to-error-scale").addClass("tc-error-spinner");
		}
	});

	$(".info-lbl-instances-status-stopped").on('click', function () {
		if ($(".tc-indicators-circle-status-stopped").hasClass("status-stopped-scale") && $(".info-lbl-instances-status-stopped").hasClass("to-stopped-scale")) {
			$(this).addClass("scaling");
			$(".lbl-status-stopped").addClass("hidden");
			$(".lbl-acting-status-stopped").removeAttr("hidden");
			$(".tc-indicators-caret-up-status-stopped").addClass("hidden");
			$(".tc-indicators-caret-down-status-stopped").addClass("hidden");
			$(".tc-indicators-circle-status-stopped.status-stopped-scale").addClass("tc-stopped-spinner");
		} 
	});

	function setStartInstancesStyle () {
		if (!$(".tc-indicators-caret-down-status-stopped").hasClass('tc-indicator-stopped-caret-down')) {
			$(".status-stopped-scale .info-quantity-instances>span").text('1');
		}

		$(".status-stopped-scale .info-lbl-instances-status-stopped>span.lbl-status-stopped").text('Start instances');

		$(".status-stopped-scale .info-lbl-instances-status-stopped").addClass('info-lbl-instances-ext');

		$(".status-stopped-scale .tc-indicators-caret-up-status-stopped").addClass("tc-indicator-stopped-caret-up");
		$(".status-stopped-scale .tc-indicators-caret-down-status-stopped").addClass("tc-indicator-stopped-caret-down");

		$(".tc-indicators-card > .tc-card-border-status-stopped").addClass("selected-stopped");

		$(".tc-indicators-card .tc-indicators-circle-status-stopped").addClass("tc-circle-no-border");

		var baseElem = ".tc-indicators-circle-status-stopped.status-stopped-scale";
		var instances = getQtyInstances(baseElem);

		if (instances > 0 && instances <= 3) {
			addStartClass(".tc-indicators-circle-status-stopped.status-stopped-scale");
		} else {
			removeStartClass(".tc-indicators-circle-status-stopped.status-stopped-scale");
		}
	}

	function setStartStyleHover(elem) {
		if (!$(elem).hasClass('status-stopped-scale')) {
			infoQty = infoQty == '' 
				? $(".tc-indicators-circle-status-stopped .info-quantity-instances>span").text() : infoQty;
			infoLbl = infoLbl == '' 
				? $(".tc-indicators-circle-status-stopped .info-lbl-instances-status-stopped>span.lbl-status-stopped").text() : infoLbl;
			$(elem).removeClass('status-stopped').addClass('status-stopped-start');
			$(".status-stopped-start .info-quantity-instances>span").text('Start');
			$(".status-stopped-start .info-lbl-instances-status-stopped>span.lbl-status-stopped").text('Start instances');
			$(".status-stopped-start .info-lbl-instances-status-stopped").addClass('info-lbl-instances-ext');
		}
	}

	function setStartStyleUnhover(elem) {
		if (!$(elem).hasClass('status-stopped-scale')) {
			$(".status-stopped-start .info-lbl-instances-status-stopped").removeClass('info-lbl-instances-ext');
			$(elem).removeClass('status-stopped-start').addClass('status-stopped');
			$(".tc-indicators-circle-status-stopped .info-quantity-instances>span").text(infoQty);
			$(".tc-indicators-circle-status-stopped .info-lbl-instances-status-stopped>span.lbl-status-stopped").text(infoLbl);
			infoQty = '';
			infoLbl = '';
		}
	}

	function scaleUp(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances++;

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + " .info-quantity-instances>span").text(instances);
			if (instances > 0) {
				$(elem + " ." + iconClass).removeClass("disabled");
				$(elem + " .info-lbl-instances-status-stopped").removeClass("disabled");
				$(elem + " .info-lbl-instances-ext").removeClass("disabled");
			}
			// add Scale class if needed
			addScaleClass(elem);
			// add Start class if needed
			addStartClass(elem);
			// add Start class if needed
			addScaleFromErrorClass(elem);
		} else {
			if (!$(elem + " ." + iconClassUp).hasClass("disabled")) {
				$(elem + " ." + iconClassUp).addClass("disabled");
			}
		}
		// Verify Caret on Scale Up
		verifyCaretOnScaleUp(elem, instances);
	}

	function scaleUpRunning(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances++;

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + " .info-quantity-instances>span").text(instances);
			if (instances > 0) {
				$(elem + " ." + iconClass).removeClass("disabled");
				$(elem + " .info-lbl-instances-status-running").removeClass("disabled");
				$(elem + " .info-lbl-instances-ext").removeClass("disabled");
			}
			// add Scale class if needed
			addScaleClass(elem);
			// add Start class if needed
			addStartClass(elem);
			// add Start class if needed
			addScaleFromErrorClass(elem);
		} else {
			if (!$(elem + " ." + iconClassUp).hasClass("disabled")) {
				$(elem + " ." + iconClassUp).addClass("disabled");
			}
		}
		// Verify Caret on Scale Up
		verifyCaretOnScaleUp(elem, instances);
	}

	function scaleUpError(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances++;

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + " .info-quantity-instances>span").text(instances);
			if (instances > 0) {
				$(elem + " ." + iconClass).removeClass("disabled");
				$(elem + " .info-lbl-instances-status-error").removeClass("disabled");
				$(elem + " .info-lbl-instances-ext").removeClass("disabled");
			}
			// add Scale class if needed
			addScaleClass(elem);
			// add Start class if needed
			addStartClass(elem);
			// add Start class if needed
			addScaleFromErrorClass(elem);
		} else {
			if (!$(elem + " ." + iconClassUp).hasClass("disabled")) {
				$(elem + " ." + iconClassUp).addClass("disabled");
			}
		}
		// Verify Caret on Scale Up
		verifyCaretOnScaleUp(elem, instances);
	}

	function scaleDown(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances--;

		if (instances <= 0) {
			removeScaleClass(elem);
			removeStartClass(elem);
			removeScaleFromErrorClass(elem);
			$(elem + " ." + iconClass).addClass("disabled");
			$(elem + " .info-lbl-instances-status-stopped").addClass("disabled");
			$(elem + " .info-lbl-instances-ext").addClass("disabled");
		} else {
			$(elem + " ." + iconClass).removeClass("disabled");
			$(elem + " .info-lbl-instances-status-stopped").removeClass("disabled");
		}

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + "  .info-quantity-instances>span").text(instances);
			$(elem + " ." + iconClassUp).removeClass("disabled");
		} else {
			$(elem + "  .info-quantity-instances>span").text('0');
		}
		// Verify Caret on Scale Down
		verifyCaretOnScaleDown(elem, instances);
	}

	function scaleDownRunning(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances--;

		if (instances <= 0) {
			removeScaleClass(elem);
			removeStartClass(elem);
			removeScaleFromErrorClass(elem);
			$(elem + " ." + iconClass).addClass("disabled");
			$(elem + " .info-lbl-instances-status-running").addClass("disabled");
			$(elem + " .info-lbl-instances-ext").addClass("disabled");
		} else {
			$(elem + " ." + iconClass).removeClass("disabled");
			$(elem + " .info-lbl-instances-status-running").removeClass("disabled");
		}

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + "  .info-quantity-instances>span").text(instances);
			$(elem + " ." + iconClassUp).removeClass("disabled");
		} else {
			$(elem + "  .info-quantity-instances>span").text('0');
		}
		// Verify Caret on Scale Down
		verifyCaretOnScaleDown(elem, instances);
	}

	function scaleDownError(elem, iconClass, iconClassUp) {
		instances = getQtyInstances(elem);
		instances--;

		if (instances <= 0) {
			removeScaleClass(elem);
			removeStartClass(elem);
			removeScaleFromErrorClass(elem);
			$(elem + " ." + iconClass).addClass("disabled");
			$(elem + " .info-lbl-instances-status-error").addClass("disabled");
			$(elem + " .info-lbl-instances-ext").addClass("disabled");
		} else {
			$(elem + " ." + iconClass).removeClass("disabled");
			$(elem + " .info-lbl-instances-status-error").removeClass("disabled");
		}

		if (instances >= 0 && instances <= 3) {
			instances = String(instances);
			$(elem + "  .info-quantity-instances>span").text(instances);
			$(elem + " ." + iconClassUp).removeClass("disabled");
		} else {
			$(elem + "  .info-quantity-instances>span").text('0');
		}
		// Verify Caret on Scale Down
		verifyCaretOnScaleDown(elem, instances);
	}

	function getQtyInstances(elem) {
		var instances = $(elem + " .info-quantity-instances>span").text();
		var pieces = instances.split("\/");
		instances = pieces[0];
		instances = Number(instances);
		return instances;
	}

	function addScaleClass(elem) {
		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			$(elem + " .info-lbl-instances-status-running>span.lbl-status-running").text("Scale");
			$(elem + " .info-lbl-instances-status-running.info-lbl-instances-ext").addClass('to-running-scale');
		}
	}

	function removeScaleClass(elem) {
		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			$(elem + " .info-lbl-instances-status-running>span.lbl-status-running").text("Scale Instances");
			$(elem + " .info-lbl-instances-status-running.info-lbl-instances-ext").removeClass('to-running-scale');
		}
	}

	function addStartClass(elem) {
		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			$(elem + " .info-lbl-instances-status-stopped>span.lbl-status-stopped").text("Scale");
			$(elem + " .info-lbl-instances-status-stopped.info-lbl-instances-ext").addClass('to-stopped-scale');
		}
	}

	function removeStartClass(elem) {
		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			$(elem + " .info-lbl-instances-status-stopped>span.lbl-status-stopped").text("Start instances");
			$(elem + " .info-lbl-instances-status-stopped.info-lbl-instances-ext").removeClass('to-stopped-scale');
		}
	}

	function addScaleFromErrorClass(elem) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			$(elem + " .info-lbl-instances-status-error>span.lbl-status-error").text("Scale");
			$(elem + " .info-lbl-instances-status-error.info-lbl-instances-ext").addClass('to-error-scale');
		}
	}

	function removeScaleFromErrorClass(elem) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			$(elem + " .info-lbl-instances-status-error>span.lbl-status-error").text("Start instances");
			$(elem + " .info-lbl-instances-status-error.info-lbl-instances-ext").removeClass('to-error-scale');
		}
	}

	function verifyCaretOnScaleUp(elem, instances) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			if (instances == 3) {
				$(".tc-indicators-caret-up-status-error.tc-indicator-error-caret-up")
					.addClass("tc-indicator-disabled-caret-up");
			} else {
				$(".tc-indicators-caret-up-status-error.tc-indicator-error-caret-up")
					.removeClass("tc-indicator-disabled-caret-up");
			}
			$(".tc-indicators-caret-down-status-error.tc-indicator-error-caret-down")
				.removeClass("tc-indicator-disabled-caret-down");
		}

		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			if (instances == 3) {
				$(".tc-indicators-caret-up-status-running.tc-indicator-running-caret-up")
					.addClass("tc-indicator-disabled-caret-up");
			} else {
				$(".tc-indicators-caret-up-status-running.tc-indicator-running-caret-up")
					.removeClass("tc-indicator-disabled-caret-up");
			}
			$(".tc-indicators-caret-down-status-running.tc-indicator-running-caret-down")
				.removeClass("tc-indicator-disabled-caret-down");
		}

		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			if (instances == 3) {
				$(".tc-indicators-caret-up-status-stopped.tc-indicator-stopped-caret-up")
					.addClass("tc-indicator-disabled-caret-up");
			} else {
				$(".tc-indicators-caret-up-status-stopped.tc-indicator-stopped-caret-up")
					.removeClass("tc-indicator-disabled-caret-up");
			}
			$(".tc-indicators-caret-down-status-stopped.tc-indicator-stopped-caret-down")
				.removeClass("tc-indicator-disabled-caret-down");
		}
	}

	function verifyCaretOnScaleDown(elem, instances) {
		if (elem == ".tc-indicators-circle-status-error.status-to-error-scale") {
			if (instances == 0) {
				$(".tc-indicators-caret-down-status-error.tc-indicator-error-caret-down")
					.addClass("tc-indicator-disabled-caret-down");
			} else {
				$(".tc-indicators-caret-down-status-error.tc-indicator-error-caret-down")
					.removeClass("tc-indicator-disabled-caret-down");
			}
			$(".tc-indicators-caret-up-status-error.tc-indicator-error-caret-up")
				.removeClass("tc-indicator-disabled-caret-up");
		}

		if (elem == ".tc-indicators-circle-status-running.status-to-running-scale") {
			if (instances == 0) {
				$(".tc-indicators-caret-down-status-running.tc-indicator-running-caret-down")
					.addClass("tc-indicator-disabled-caret-down");
			} else {
				$(".tc-indicators-caret-down-status-running.tc-indicator-running-caret-down")
					.removeClass("tc-indicator-disabled-caret-down");
			}
			$(".tc-indicators-caret-up-status-running.tc-indicator-running-caret-up")
				.removeClass("tc-indicator-disabled-caret-up");
		}

		if (elem == ".tc-indicators-circle-status-stopped.status-stopped-scale") {
			if (instances == 0) {
				$(".tc-indicators-caret-down-status-stopped.tc-indicator-stopped-caret-down")
					.addClass("tc-indicator-disabled-caret-down");
			} else {
				$(".tc-indicators-caret-down-status-stopped.tc-indicator-stopped-caret-down")
					.removeClass("tc-indicator-disabled-caret-down");
			}
			$(".tc-indicators-caret-up-status-stopped.tc-indicator-stopped-caret-up")
				.removeClass("tc-indicator-disabled-caret-up");
		}
	}
});