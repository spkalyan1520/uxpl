$(function(){
	$(".tc-table>tbody>tr").on('click', function(){
		$(this).addClass('selected').siblings().removeClass('selected');
        if($(this).hasClass("checked")) {
            $(this).removeClass('selected');
        }
	});
});

$(function(){
    $(".tc-table>tbody>tr>td>div>input[type=checkbox-deleted]+label").click(function(){
        var mElem = $(this).parents("tr");
        if(!$(this).prev().is(":checked")) {
            mElem.removeClass('selected').addClass('checked');
        } else {
            mElem.removeClass('selected checked');
        }
    });
});

$(function(){
    $('input[type=checkbox-deleted].tc-table-checkbox-deleted-select-all').on('click',function(){
        if(this.checked){
            toggleCheck(true);
            $('.tc-table>tbody>tr').each(function(){
                $(this).removeClass('selected').addClass('checked');
            });
        }else{
            toggleCheck(false);
            $('.tc-table>tbody>tr').each(function(){
                $(this).removeClass('selected checked');
            });
        }
    });
    $('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]').on('click',function(){
        if($('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]:checked').length == $('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]').length){
            $('input[type=checkbox-deleted].tc-table-checkbox-deleted-select-all').prop('checked',true);
        }else{
            $('input[type=checkbox-deleted].tc-table-checkbox-deleted-select-all').prop('checked',false);
        }
    });
});

function toggleCheck(value) {
    $('.tc-table>tbody>tr>td>div>input[type=checkbox-deleted]').each(function(){
        this.checked = value;
    });
};