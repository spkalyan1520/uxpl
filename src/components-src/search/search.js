/**
 * Created by hede on 9/29/2017.
 */

function matchCountry(element, input) {
    var data = eval(element.getAttribute("data-var"));
    var reg = new RegExp(input.split('').join('\\w*').replace(/\W/, ""), 'i');
    if (data) {
        return data.filter(function (country) {
            if (country.value.match(reg)) {
                return country;
            }
        });
    } else {
        return '';
    }
}

function changeInput(element, val) {
    var autoCompleteResult = matchCountry(element, val);
    var temp = '';
    autoCompleteResult.forEach(function (entry) {
        temp += "<li onclick='selectItem(this)'>" + entry.value + "</li>";
    });
    document.getElementById("result").innerHTML = temp;
    document.getElementById("result").style.display = 'block';
}

function selectItem(item) {
    document.querySelector('.tc-search .tc-search-container > input').value = item.textContent;
    document.getElementById("result").style.display = 'none';
}

